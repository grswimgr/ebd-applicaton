package utils;

import engine.DiagramEngineController;
import javafx.scene.control.Alert;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.log4j.Logger;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static utils.UTIL_Methods.handleExceptionShowAlert;

public class UTIL_DataTransfer {
    private static final Logger LOGGER = Logger.getLogger( UTIL_DataTransfer.class.getName() );

    public static class DataTransferException extends Exception {
        public DataTransferException(String message) {
            super(message);
        }
    }

    public static String getFileContent(String path) throws FileNotFoundException {
        Scanner s = null;
        String result = "";
        File file = new File(path);
        try {
            s = new Scanner(new BufferedReader(new FileReader(file.toString())));
            result += s.nextLine();
            while (s.hasNext()) {
                result += s.nextLine().toString();
            }
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return result;
    }

    public static void saveFileContent(String path, String body) {
        try {
            LOGGER.info("Saving file... File Path: " + path);

            try (PrintWriter out = new PrintWriter(path)) {
                LOGGER.trace("File body: \n" + body);
                out.println(body);
            }
        } catch (Exception e) {
            handleExceptionShowAlert(Alert.AlertType.ERROR, "File saving error", e);
        }
    }

    public static void exportResultsToCSV(String path, DiagramEngineController dac) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
            LocalDateTime now = LocalDateTime.now();

            String fileName = dac.getEngineName() + " " + now.format(dtf) + ".csv";

            Map<String,  Object> resultWrapper  = dac.getResultsToExport();
            List<String> headers = (List<String>)resultWrapper.get("HEADERS");
            Map<Double, Double[]> results = (Map<Double, Double[]>)resultWrapper.get("VALUES");

            CSVPrinter printer = new CSVPrinter(new FileWriter(path + "/" + fileName), CSVFormat.EXCEL);
            printer.printRecord(headers);

            for(Double x : results.keySet()) {
                List<Double> values = new ArrayList<>();
                values.add(x);
                values.addAll(Arrays.asList(results.get(x)));
                printer.printRecord(values);
            }

            printer.println();

            UTIL_Methods.showPopup(Alert.AlertType.CONFIRMATION, "File saved Successfully!", "Path: " + path + "/" + fileName);
        } catch (Exception e) {
            handleExceptionShowAlert(Alert.AlertType.ERROR, "File saving error", e);
        }
    }
}
