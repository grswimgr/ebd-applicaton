package utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import helpers.SelectStructureWindowHelper;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Structure;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Stream;

public class UTIL_StructureComposer {
    public static class StructureComposerException extends Exception {
        public StructureComposerException(String message) {
            super(message);
        }
    }

    public static void saveStructureJSON(Structure structure) {
        System.out.println("In saving handler");
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String structureString = objectMapper.writeValueAsString(structure);

            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Structure definition files (*.sdf)", "*.sdf");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(new Stage());

            if(file != null){
                saveFile(structureString, file);
            }

            UTIL_Methods.showPopup(Alert.AlertType.INFORMATION, "File saved Successfully!", "Path: " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot Save Structure", e);
        }
    }

    public static Structure loadStructureFromFile() {
        try {
            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Structure definition files (*.sdf)", "*.sdf");
            fileChooser.getExtensionFilters().add(extFilter);

            File file = fileChooser.showOpenDialog(new Stage());
            return loadStructureFromFile(file);
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot load selected file!", e);
            return null;
        }
    }

    public static Structure loadStructureFromFile(File file) {
        try {
            if (file != null) {
                rebuildAndSaveRecentFilesList(file);

                String fileContent = UTIL_DataTransfer.getFileContent(file.getPath());
                Structure structure = new ObjectMapper().readValue(fileContent, Structure.class);

                return structure;
            } else {
                return null;
            }
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot load selected file!", e);
            return null;
        }
    }

    public static void rebuildAndSaveRecentFilesList(File fileToAdd) {
        try {
            // Remove duplicates
            File[] actualFiles = SelectStructureWindowHelper.prepareRecentlyUsedFiles();
            Set<File> allFiles = new HashSet<File>(Arrays.asList(actualFiles));
            allFiles.add(fileToAdd);

            Calendar cal = Calendar.getInstance();
            fileToAdd.setLastModified(cal.getTimeInMillis());

            Map<Long, String> pathsMap = new TreeMap<>(Collections.reverseOrder());
            for(File f : allFiles) {
                pathsMap.put(f.lastModified(), f.getPath());
            }

            String output = null;
            Integer counter = 0;


            for(String path : pathsMap.values()) {
                if(counter == 10) break;
                output = (output == null) ? path : output + "\n" + path;
                counter++;
            }

            // Save results to file
            UTIL_DataTransfer.saveFileContent("recentlyUsed.txt", output);

        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot open recently used file", e);
        }
    }

    public static void saveFile(String content, File file){
        try {
            FileWriter fileWriter = null;
            fileWriter = new FileWriter(file);
            fileWriter.write(content);
            fileWriter.close();
        } catch (IOException ex) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot Save file!", ex);
        }
    }
}
