package utils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.EngineSelectorController;
import controllers.StructureComposerController;
import ebd.EngineLoader;
import engine.DiagramEngineController;
import helpers.SelectEngineWindowHelper;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.web.WebView;
import model.*;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

//import static utils.UTIL_Runtime.CUSTOM_DIAGRAM_ENGINE_HANDLER;
import static utils.UTIL_Runtime.ENGINE_CONTROLLER_HANDLERS;

public class UTIL_Methods {

    private static final Logger LOGGER = Logger.getLogger( UTIL_Methods.class.getName() );

    public static void handleExceptionShowAlert(Alert.AlertType type, String alertHeader, Exception e) {
        Alert errorAlert = new Alert(type);
        errorAlert.setHeaderText(alertHeader);
        errorAlert.setContentText(e.getMessage() + "\n" + e.getCause() + "\n" + String.valueOf(e.getStackTrace()));

        String errorMessage = e.getMessage() + "\n\n";

        String showExceptionDetails = UTIL_Runtime.APPLICATION_SETTINGS.getProperty("general.showExceptionDetails");
        if(showExceptionDetails != null && showExceptionDetails.equalsIgnoreCase("true")) {
            errorMessage += "Technical details: \n";
            errorMessage += (e.getClass() != null) ? "Exception class: " + e.getClass().getName() + "\n\n": "";
            for(StackTraceElement ste : e.getStackTrace()) {
                errorMessage += ste + "\n";
            }
        }

        errorAlert.setContentText(errorMessage);
        errorAlert.showAndWait();

        LOGGER.error(errorMessage);
    }

    public static DiagramEngineController generateDiagramEngineController(String path, String name) {
        DiagramEngineController loadedController = null;
        try {
            EngineLoader el = new EngineLoader(path);
            loadedController = (DiagramEngineController) el.getLoadedEngine();
            ENGINE_CONTROLLER_HANDLERS.put(name, loadedController);
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot initialize calculation model.", e);
        }
        return loadedController;
    }


    public static Properties loadProperties(String path) {
        try {
            LOGGER.trace("Loading properties details: " + path);
            File file = new File(path);
            InputStream is = new FileInputStream(file);
            Properties loadedProperties = new Properties();
            loadedProperties.load(is);
            return loadedProperties;
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.WARNING, "Cannot load file: " + path, e);
        }

        return null;
    }

    public static void showPopup(Alert.AlertType type, String title, String content) {
        Alert errorAlert = new Alert(type);
        errorAlert.setHeaderText(title);
        errorAlert.setContentText(content);
        errorAlert.showAndWait();
    }

    public static void openAboutAppModal() {
        String content = "This application was created as a part of the master's thesis.\n";
        content += "\n\n";
        content += "Author: Grzegorz Świnarski\n";
        content += "2021\n";

        showPopup(Alert.AlertType.INFORMATION, "About EBD", content);
    }
}
