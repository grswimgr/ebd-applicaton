package utils;

import engine.DiagramEngineController;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Material;
import model.Structure;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Properties;


public class UTIL_Runtime {
    private static final Logger LOGGER = Logger.getLogger( UTIL_Runtime.class.getName() );

    public static Stage MAIN_WINDOW_STAGE;
    public static Scene SELECT_ENGINE_SECENE, SELECT_STRUCTURE_SECENE, STRUCTURE_COMPOSER_SCENE, LAYER_PROPERTIES_SCENE, MATERIAL_PROPERTIES_SCENE, SIMULATION_SCENE, LOADER_SCENE;
    public static Structure MODEL_STRUCTURE;

    public static HashMap<String, DiagramEngineController> ENGINE_CONTROLLER_HANDLERS = new HashMap<>();

    public static Properties APPLICATION_SETTINGS;

    public static HashMap<String, Material> MATERIALS_MAP = new HashMap<String, Material>();

    public static void loadApplicationSettins() {
        LOGGER.debug("Loading application settings...");
        APPLICATION_SETTINGS = UTIL_Methods.loadProperties("settings.properties");
        LOGGER.debug("Loading application settings... [Done]");
    }
}
