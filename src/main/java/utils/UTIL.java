package utils;

public class UTIL {
    public enum MaterialType {
        METAL, DIELECTRIC, SEMICONDUCTOR;
    }

    public enum SimulationType {
        ENERGY, POTENTIAL, ELECTRIC_FIELD, CHARGE_DENSITY
    }

    public enum CommonPartType {
        LEFT, RIGHT, INSIDE, OUTSIDE, ERROR;
    }
}
