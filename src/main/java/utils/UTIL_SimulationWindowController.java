package utils;

import engine.DiagramEngineController;
import helpers.SelectEngineWindowHelper;
import javafx.scene.control.Alert;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class UTIL_SimulationWindowController {
    private static final Logger LOGGER = Logger.getLogger( UTIL_SimulationWindowController.class.getName() );

    public static UTIL.SimulationType getSimulationTypeFromString(String simulationType) {
        switch(simulationType) {
            case "Energy":
                return UTIL.SimulationType.ENERGY;
            case "Charge Density":
                return UTIL.SimulationType.CHARGE_DENSITY;
            case "Electric Field":
                return UTIL.SimulationType.ELECTRIC_FIELD;
            case "Potential":
                return UTIL.SimulationType.POTENTIAL;
            default:
                return null;
        }
    }

    public static List<File> prepareEnginesPropertiesFilesList() throws IOException {
        List<File> propertiesFilesToProcess = new ArrayList<>();

        LOGGER.info("Searching available Engines...");
        Files.find(new File("engines").toPath(),Integer.MAX_VALUE,(filePath, fileAttr) -> fileAttr.isDirectory()).forEach(
                path -> {
                    File[] propertiesFiles = path.toFile().listFiles(new FilenameFilter() {
                        public boolean accept(File dir, String name) {
                            return name.endsWith("properties");
                        }
                    });
                    propertiesFilesToProcess.addAll(Arrays.asList(propertiesFiles));
                }
        );

        LOGGER.debug("Found engines properties:");
        for(File file : propertiesFilesToProcess) {
            LOGGER.debug(file.getPath());
        }

        return propertiesFilesToProcess;
    }

    public static List<Properties> loadEngines(List<File> filesList) {
        List<Properties> enginesProperties = new ArrayList<>();
        for(File file : filesList) {
            enginesProperties.add(UTIL_Methods.loadProperties(file.getPath()));
        }
        return enginesProperties;
    }

    public static void loadCalculationEngines() {
        try {
            List<File> enginePropertiesFiles =  prepareEnginesPropertiesFilesList();
            List<Properties> propertiesList = loadEngines(enginePropertiesFiles);

            for(Object props : propertiesList) {
                Properties properties = (Properties) props;
                DiagramEngineController loadedController = null;

                if(!properties.containsKey("engine.jar")) throw new SelectEngineWindowHelper.SelectEngineException("Invalid engine.properties file! Engine JAR path (engine.jar) is not available!");
                if(!properties.containsKey("engine.name")) throw new SelectEngineWindowHelper.SelectEngineException("Invalid engine.properties file! Engine name (engine.name) is not available!");

                if(properties.getProperty("engine.jar").startsWith("/")){
                    loadedController = UTIL_Methods.generateDiagramEngineController(properties.getProperty("engine.jar"), properties.getProperty("engine.name"));
                } else {
                    loadedController = UTIL_Methods.generateDiagramEngineController("engines/" + properties.getProperty("engine.jar"), properties.getProperty("engine.name"));
                }
                loadedController.setProperties((Properties) props);
            }
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot load engine!", e);
        }
    }
}
