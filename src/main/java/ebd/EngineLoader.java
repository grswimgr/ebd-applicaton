package ebd;

import engine.DiagramEngine;
import engine.DiagramEngineController;
import javafx.scene.control.Alert;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class EngineLoader {
    private static final org.apache.log4j.Logger LOGGER = Logger.getLogger( EngineLoader.class.getName() );

    public static class EngineLoaderException extends Exception {
        public EngineLoaderException(String message) {
            super(message);
        }
    }

    DiagramEngine loadedEngine;

    public EngineLoader(String path) {
        try {
            LOGGER.debug("Loading Engine: " + path);
            File libraryJarFile = new File(path);

            URLClassLoader child = new URLClassLoader(
                    new URL[]{libraryJarFile.toURI().toURL()},
                    this.getClass().getClassLoader()
            );

            LOGGER.trace("Child class loaded: " + child.getClass());

            Class classToLoad = Class.forName("ebdEngine.main", true, child);
            Method method = classToLoad.getDeclaredMethod("getCustomEngine");
            Object instance = classToLoad.newInstance();
            Object result = method.invoke(instance);

            DiagramEngineController modelInstance = (DiagramEngineController) result;
            this.loadedEngine  = modelInstance;
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot load ENGINE", e);
        }
    }

    public DiagramEngine getLoadedEngine() {
        return loadedEngine;
    }
}
