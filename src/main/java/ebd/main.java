package ebd;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import utils.UTIL_Runtime;
import java.io.IOException;
import org.apache.log4j.Logger;

public class main extends Application {
    private static final Logger LOGGER = Logger.getLogger( main.class.getName() );

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) {
        LOGGER.info("======================");
        LOGGER.info("APPLICATION STARTED...");
        LOGGER.info("======================");

        this.primaryStage = primaryStage; // connect primary stage
        UTIL_Runtime.MAIN_WINDOW_STAGE = primaryStage;
        UTIL_Runtime.loadApplicationSettins();
        mainWindow();
    }

    // main window
    public void mainWindow() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/selectStructureWindow.fxml"));

            VBox pane = loader.load();

            // scene on stage
            Scene scene = new Scene(pane);
            UTIL_Runtime.SELECT_ENGINE_SECENE = scene;
            UTIL_Runtime.MAIN_WINDOW_STAGE.setScene(UTIL_Runtime.SELECT_STRUCTURE_SECENE);

            primaryStage.setScene(UTIL_Runtime.SELECT_ENGINE_SECENE);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
        LOGGER.info("======================");
        LOGGER.info("APPLICATION CLOSED... ");
        LOGGER.info("======================");
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}