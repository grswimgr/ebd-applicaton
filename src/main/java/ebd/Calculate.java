package ebd;

import controllers.SimulationController;
import engine.DiagramEngineController;
import model.CalculationResult;
import model.SimulationSettings;
import java.util.List;

public class Calculate {
    DiagramEngineController engineController;
    SimulationController simulationController;
    SimulationSettings ss;
    Boolean isRunning;

    public Calculate(DiagramEngineController engineController, SimulationController simulationController, SimulationSettings ss) {
        this.engineController = engineController;
        this.simulationController = simulationController;
        this.ss = ss;
        this.isRunning = false;
    }

    public void launchCalculation() throws DiagramEngineController.DiagramEngineControllerException {
            System.out.println("THREAD RUN !");
            this.isRunning = true;
            System.out.println("SIMULATION TYPE BEFORE LAUNCH: " + this.ss.getSimulationType());

            // Run simulation
            this.engineController.createRunner(this.ss);
            this.engineController.runSimulation();
            // Convert results
            List<CalculationResult> resultList = this.engineController.getCalculationResultList();

            System.out.println("CALCULATION COMPLETED!");
            //this.simulationController.getHelper().convertCalculationResultToChartData(resultList);
            this.simulationController.getHelper().updateChartSeries();
            this.simulationController.getHelper().updateSeriesTree();

            System.out.println("CALCULATION SERIES REFRESHED!");
    }

    public Boolean getRunning() {
        return isRunning;
    }

    public void setRunning(Boolean running) {
        isRunning = running;
    }

    public DiagramEngineController getEngineController() {
        return engineController;
    }

    public void setEngineController(DiagramEngineController engineController) {
        this.engineController = engineController;
    }

    public SimulationController getSimulationController() {
        return simulationController;
    }

    public void setSimulationController(SimulationController simulationController) {
        this.simulationController = simulationController;
    }

    public SimulationSettings getSs() {
        return ss;
    }

    public void setSs(SimulationSettings ss) {
        this.ss = ss;
    }
}
