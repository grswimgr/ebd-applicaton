package engine;

public interface RunnerInterface {
    void run() throws DiagramEngineController.DiagramEngineControllerException;
}
