package engine;

import model.SimulationSettings;
import org.apache.log4j.Logger;

public class Runner implements RunnerInterface {
    public static final Logger LOGGER = Logger.getLogger(Runner.class.getName() );

    SimulationSettings ss;

    private Runner()  {}

    public Runner(SimulationSettings ss)  {
        this.ss = ss;
    }

    @Override
    public void run() throws DiagramEngineController.DiagramEngineControllerException  {
    }
}
