package engine;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyDoubleWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import model.*;
import org.apache.log4j.Logger;
import utils.UTIL;

import java.util.*;

public class DiagramEngineController implements DiagramEngine {
    public static final Logger LOGGER = Logger.getLogger(DiagramEngineController.class.getName() );
    private static final String REQUIRED_PARAMETER_MISSING = "REQUIRED_PARAMETER_MISSING";

    public static class DiagramEngineControllerException extends Exception {
        String exceptionType;
        String stackTrace;
        String message;

        public DiagramEngineControllerException(String message) {
            super(message);
        }

        public DiagramEngineControllerException(String message, String exceptionType, String stackTrace) {
            super(message);
            this.exceptionType = exceptionType;
            this.stackTrace = stackTrace;
            this.message = message;
        }

        public DiagramEngineControllerException(Exception e, String exceptionType) {
            super(e.getMessage());
            this.message = e.getMessage();
            this.stackTrace = e.getStackTrace().toString();
            this.exceptionType = exceptionType;
        }
    }

    protected Runner simulationRunner;
    private List<CalculationResult> calculationResults;
    private ProgressBar progressBar;
    private Label progressBarLabel;
//    protected ModelConfiguration model;
    protected Structure structure;
    private boolean isEnabled;
    private Properties properties;

    private final ReadOnlyDoubleWrapper progress = new ReadOnlyDoubleWrapper();
    private final ReadOnlyStringWrapper progressComment = new ReadOnlyStringWrapper();

    public double getProgress() {
        return progressProperty().get();
    }

    public String getProgressComment() {
        return progressCommentProperty().getName();
    }

    public ReadOnlyDoubleProperty progressProperty() {
        return progress ;
    }

    public ReadOnlyStringWrapper progressCommentProperty() {
        return progressComment ;
    }

    public DiagramEngineController() throws DiagramEngineControllerException {
        LOGGER.info("Loading Model Configuration File...");
        LOGGER.info("Cache initialization...");
        calculationResults = new ArrayList<>();
    };

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public List<CalculationResult> getCalculationResults() {
        return calculationResults;
    }

    public void setCalculationResults(List<CalculationResult> calculationResults) {
        this.calculationResults = calculationResults;
    }

    protected HashMap<UTIL.MaterialType, HashMap<String, String>> requiredParametersMap;

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public static String getRequiredParameterMissing() {
        return REQUIRED_PARAMETER_MISSING;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public Label getProgressBarLabel() {
        return progressBarLabel;
    }

    public void setProgressBarLabel(Label progressBarLabel) {
        this.progressBarLabel = progressBarLabel;
    }

    public Structure getStructure() {
        return structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }


    public HashMap<UTIL.MaterialType, HashMap<String, String>> getRequiredParametersMap() {
        return requiredParametersMap;
    }

    public void setRequiredParametersMap(HashMap<UTIL.MaterialType, HashMap<String, String>> requiredParametersMap) {
        this.requiredParametersMap = requiredParametersMap;
    }

    public void prepareRequiredParametersMap() {
        this.requiredParametersMap = new HashMap<>();
    }

    public void validateStructureParameters() throws DiagramEngineController.DiagramEngineControllerException {
        validateStructureParameters(UTIL.MaterialType.METAL, this.requiredParametersMap.get(UTIL.MaterialType.METAL));
        validateStructureParameters(UTIL.MaterialType.SEMICONDUCTOR, this.requiredParametersMap.get(UTIL.MaterialType.SEMICONDUCTOR));
        validateStructureParameters(UTIL.MaterialType.DIELECTRIC, this.requiredParametersMap.get(UTIL.MaterialType.DIELECTRIC));
    }

    public void validateStructureParameters(UTIL.MaterialType materialType, HashMap<String, String> requiredParametersMap) throws DiagramEngineControllerException {
        for(Layer layer : this.structure.getLayers()) {
            Material material = layer.getMaterial();
            if(material.getType() == materialType) {
                // Prepare Name -> Token map
                HashMap<String, String> parameterTokenToNameMap = new HashMap<>();
                for(Parameter p : material.getParameters().values()) {
                    parameterTokenToNameMap.put(p.getToken(), p.getName());
                }

                for(String requiredParameterToken : requiredParametersMap.keySet()) {
                    LOGGER.trace("Checking token: " + requiredParameterToken);
                    LOGGER.trace("Checking token: " + parameterTokenToNameMap.keySet());

                    if(!parameterTokenToNameMap.containsKey(requiredParameterToken)) {
                        throw new DiagramEngineControllerException(materialType.toString() + ": Required parameter is missing: " + requiredParametersMap.get(requiredParameterToken) +  "(Token: " + requiredParameterToken + ").",REQUIRED_PARAMETER_MISSING, null);
                    }
                }
            }
        }
    }

    @Override
    public String getEngineName() {
        return "Default Application engine";
    }

    public DiagramEngineController getControllerInstance() {
        System.out.println("Returing instance of DiagramEngineController: Main Application");
        return this;
    }

    public void setControllerParameters(Structure structure) {
        this.structure = structure;
    }

    public void enableProgressBar(ProgressBar progressBar, Label progressBarLabel) {
        this.progressBar = progressBar;
        this.progressBarLabel = progressBarLabel;
    }

    @Override
    public void refreshProgressBar(Double progressValue, String actualStatus) {
        Platform.runLater(() -> {
            this.progressBar.setProgress(progressValue);
            this.progressBarLabel.setText(actualStatus);
        });
    }

    @Override
    public void recalculate() {

    }

    @Override
    public void createRunner(SimulationSettings simulationSettings) throws DiagramEngineControllerException {
        this.simulationRunner = new Runner(simulationSettings);
    }

    public void runSimulation() throws DiagramEngineControllerException {
        progress.set(0.1);

        LOGGER.info("Initializing custom engine details...");
        progressComment.set("Engine setup...");
        prepareEngineData();
        progress.set(0.2);
        progressComment.set("Engine setup... [Done]");

        LOGGER.info("Preparing required parameters map...");
        progressComment.set("Required parameters map preparation...");
        prepareRequiredParametersMap();
        progress.set(0.3);
        progressComment.set("Required parameters map preparation... [Done]");

        LOGGER.info("Structure validation...");
        progressComment.set("Structure validation...");
        validateStructureParameters();
        progress.set(0.4);
        progressComment.set("Structure validation [Done]");

        LOGGER.info("Structure calculation...");
        progressComment.set("Structure calculation...");

        if(simulationRunner == null) throw new DiagramEngineControllerException("Simulaltion Runner is not set!");
        this.simulationRunner.run();
        progress.set(0.8);
        progressComment.set("Structure calcullation [Done]");
    }

    public List<CalculationResult> getCalculationResultList() throws DiagramEngineControllerException {
        this.calculationResults = new ArrayList<>();

        List<CalculationResult> results = new ArrayList<CalculationResult>();


        progressComment.set("Converting: Energy...");
        results.addAll(getEnergyCalculationResultList());
        progress.set(0.85);
        progressComment.set("Converting: Energy... [Done]");

        progressComment.set("Converting: Potential...");
        results.addAll(getPotentialCalculationResultList());
        progress.set(0.90);
        progressComment.set("Converting: Potential... [Done]");

        progressComment.set("Converting: Electric Field...");
        results.addAll(getElectricFieldCalculationResultList());
        progress.set(0.95);
        progressComment.set("Converting: Electric Field... [Done]");

        progressComment.set("Converting: Charge Density...");
        results.addAll(getChargeDensityCalculationResultList());
        progress.set(1);
        progressComment.set("Converting: Charge Density... [Done]");

        this.calculationResults = results;

        return results;
    }

    public Map<String, Object> getResultsToExport() {
        String csv = "";

        List<String> headers = new ArrayList<>();
        headers.add("x");
        Map<Double, Double[]> results = new TreeMap<>();
        Integer resultRows = this.calculationResults.size();


        for(CalculationResult result : this.calculationResults) {
            headers.add(result.getSimulationType() + " - " + result.getMaterialName() + " - " + result.getChartSeries().getName());
            XYChart.Series series = result.getChartSeries();

            ObservableList<XYChart.Data> observableArrayList = FXCollections.observableArrayList(series.getData());
            Integer seriesIndex = this.calculationResults.indexOf(result);
            for(XYChart.Data point :observableArrayList) {
                Double xVal = (Double) point.getXValue();
                Double yVal = (Double) point.getYValue();

                if(!results.containsKey(xVal)) {
                    results.put(xVal, new Double[resultRows]);
                }

                results.get(xVal)[seriesIndex] = yVal;
            }
        }

        Map<String,  Object> resultWrapper = new HashMap<>();
        resultWrapper.put("HEADERS", headers);
        resultWrapper.put("VALUES", results);
        return resultWrapper;
    }


    public void prepareEngineData() throws DiagramEngineControllerException {
    }

    @Override
    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getPropertyValue(String key) {
        return String.valueOf(this.properties.get(key));
    }

    public List<CalculationResult> getEnergyCalculationResultList() throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type (Energy) not supported in selected model!");
    }

    public List<CalculationResult> getPotentialCalculationResultList() throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type (Potential) not supported in selected model!");
    }

    public List<CalculationResult> getElectricFieldCalculationResultList() throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type (Electric Field) not supported in selected model!");
    }

    public List<CalculationResult> getChargeDensityCalculationResultList() throws DiagramEngineControllerException {
        throw new DiagramEngineControllerException("Simulation Type (Charge Density) not supported in selected model!");
    }
}
