package engine;

import model.*;
import utils.UTIL;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public interface DiagramEngine {
    void refreshProgressBar(Double progressValue, String actualStatus);
    void prepareRequiredParametersMap() throws DiagramEngineController.DiagramEngineControllerException;
    void validateStructureParameters() throws DiagramEngineController.DiagramEngineControllerException;
    void validateStructureParameters(UTIL.MaterialType materialType, HashMap<String, String> requiredParameters) throws DiagramEngineController.DiagramEngineControllerException;

    String getEngineName();

    void recalculate();

    void prepareEngineData() throws DiagramEngineController.DiagramEngineControllerException;
    void createRunner(SimulationSettings simulationSettings) throws DiagramEngineController.DiagramEngineControllerException;
    void setProperties(Properties properties);

    void runSimulation() throws DiagramEngineController.DiagramEngineControllerException;
    List<CalculationResult> getEnergyCalculationResultList() throws DiagramEngineController.DiagramEngineControllerException;
    List<CalculationResult> getPotentialCalculationResultList() throws DiagramEngineController.DiagramEngineControllerException;
    List<CalculationResult> getElectricFieldCalculationResultList() throws DiagramEngineController.DiagramEngineControllerException;
    List<CalculationResult> getChargeDensityCalculationResultList() throws DiagramEngineController.DiagramEngineControllerException;
}
