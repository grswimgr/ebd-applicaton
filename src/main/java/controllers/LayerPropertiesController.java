package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.ExtraCharge;
import model.Layer;
import model.Parameter;
import org.apache.log4j.Logger;
import utils.UTIL;
import utils.UTIL_Methods;

import java.io.IOException;

public class LayerPropertiesController {
    private static final Logger LOGGER = Logger.getLogger( LayerPropertiesController.class.getName() );

    @FXML public TextField layerName;
    @FXML public TextField materialType;
    @FXML public TextArea layerDescription;
    @FXML public ListView layerParametersList;
    @FXML public VBox layerProperties;
    @FXML public Label labelPosition;
    @FXML public TextField positionValue;
    @FXML public VBox chargeVbox;
    @FXML public ListView extraChargeList;
    @FXML public TitledPane extraChargeSection;

    private Layer layer;
    private StructureComposerController scc;
    private ObservableList<Parameter> parametersObservableList;
    private ObservableList<ExtraCharge> chargeObservableList;

    public void initData(Layer layer, StructureComposerController scc) {
        this.layer = layer;
        this.scc = scc;

        initLayerParametersList();
        initExtraChargeList();
        initLayerFieldsValues();
    }

    public Double getTotalThicknessBeforeLayer(Layer layer) {
        Double totalThickness = 0.0;

        for(Layer l : this.scc.getStructure().getLayers()) {
            if(l != layer) {
                totalThickness += l.getThickness();
            } else {
                return totalThickness;
            }
        }
        return totalThickness;
    }

    public void initLayerParametersList() {
        parametersObservableList = FXCollections.observableArrayList(layer.getMaterial().getParameters().values());
        chargeObservableList = FXCollections.observableArrayList(layer.getExtraCharge().values());

        parametersObservableList.addListener(new ListChangeListener<Parameter>() {
            public void onChanged(Change<? extends Parameter> c) {
                LOGGER.trace("Parameter List change detected!  Refreshing Viev...");
                while (c.next()) {
                    layer.getMaterial().getParameters().clear();
                    for (Parameter permutatedItem : c.getList()) {
                        layer.getMaterial().getParameters().put(permutatedItem.getToken(), permutatedItem);

                        if(permutatedItem.getName().equalsIgnoreCase("thickness")) {
                            layer.setThickness(permutatedItem.getValue());
                        }
                    }
                    refreshLayerParametersList();
                }
            }
        });

        this.layerParametersList.setCellFactory(property -> new ParameterListElementController(parametersObservableList, false));
        this.layerParametersList.setItems(parametersObservableList);
        this.layerParametersList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    //Use ListView's getSelected Item
                    Object currentItemSelected = layerParametersList.getSelectionModel().getSelectedItem();
                    new ParameterListElementController(parametersObservableList, false).openParameterEditor((Parameter) currentItemSelected);
                }
            }
        });
    }

    public void initExtraChargeList() {
        if(!layer.getMaterial().getType().equals(UTIL.MaterialType.DIELECTRIC)) {
            this.extraChargeSection.setVisible(false);
        }

        chargeObservableList = FXCollections.observableArrayList(layer.getExtraCharge().values());

        chargeObservableList.addListener(new ListChangeListener<ExtraCharge>() {
            public void onChanged(Change<? extends ExtraCharge> c) {
                LOGGER.trace("Parameter List change detected!  Refreshing Viev...");
                while (c.next()) {
                    layer.getExtraCharge().clear();
                    for (ExtraCharge permutatedItem : c.getList()) {
                        layer.getExtraCharge().put(permutatedItem.getPosition(), permutatedItem);
                    }
                    initExtraChargeList();
                }
            }
        });

        this.extraChargeList.setCellFactory(property -> new ExtraChargeListElementController(chargeObservableList, scc.getStructure(), layer));
        this.extraChargeList.setItems(chargeObservableList);
        this.extraChargeList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    //Use ListView's getSelected Item
                    Object currentItemSelected = extraChargeList.getSelectionModel().getSelectedItem();
                    new ExtraChargeListElementController(chargeObservableList, scc.getStructure(), layer).openParameterEditor((ExtraCharge) currentItemSelected);
                }
            }
        });
    }

    public void refreshLayerParametersList() {
        initLayerParametersList();
    }

    public void initLayerFieldsValues() {
        this.layerName.setText(this.layer.getName());
        this.layerDescription.setText(this.layer.getDescription());
        this.materialType.setText(this.layer.getMaterial().getType().toString());
    }

    public void saveLayerHandler(MouseEvent mouseEvent) {
        LOGGER.trace("Handle: saveLayer");
        mapFrontToObjectFields();

        // Enforce parent refresh
        this.scc.refreshCmpFields();
        this.scc.initData();
        this.scc.initTableColumnsMapping();
        this.scc.helper.refreshStructureChart();

        // Close modal
        Stage scene = (Stage) this.layerName.getScene().getWindow();
        scene.close();
    }

    public void mapFrontToObjectFields() {
        LOGGER.trace("Handle: mapFrontToObjectFields");
        this.layer.setName(this.layerName.getText());
        this.layer.setDescription(this.layerDescription.getText());
        saveExtraChargeData();
    }

    public void saveExtraChargeData() {
        //this.layer.setExtraEndLayerCharge(Double.valueOf(this.positionValue.getText()));
    }

    public void addParameterHandler(MouseEvent mouseEvent) {
        openLayerParameterEditor(null);
    }

    public void addExtraChargeHandler(MouseEvent mouseEvent) {
        openExtraChargeEditor(null);
    }

    public void openLayerParameterEditor(Parameter parameter) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/layerParameter.fxml"));
            VBox pane = root.load();
            LayerParameterController lpc = root.getController();
            lpc.initData(parametersObservableList, parameter);
            Stage secondStage  = new Stage();
            secondStage.setScene(new Scene(pane));
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.show();
        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Parameter Scene", e);
        }
    }

    public void openExtraChargeEditor(ExtraCharge charge) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/layerExtraCharge.fxml"));
            VBox pane = root.load();
            LayerExtraChargeController lec = root.getController();
            lec.initData(chargeObservableList, charge, scc.getStructure(), layer);
            Stage secondStage  = new Stage();
            secondStage.setScene(new Scene(pane));
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.show();
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Parameter Scene", e);
        }
    }

    public Layer getLayer() {
        return layer;
    }

    public void cancelHandler(MouseEvent mouseEvent) {
        Stage scene = (Stage) this.layerName.getScene().getWindow();
        scene.close();
    }
}
