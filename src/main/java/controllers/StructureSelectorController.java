package controllers;

import helpers.SelectStructureWindowHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import model.Structure;
import utils.UTIL_Methods;
import utils.UTIL_Runtime;
import utils.UTIL_StructureComposer;


public class StructureSelectorController {
    @FXML public TableView recentlyUsedTable;
    @FXML public Text materialPath;

    SelectStructureWindowHelper helper;

    public void initialize() {
        System.out.println("Loading Custom Model Details...");
        helper = new SelectStructureWindowHelper(this);
        recentlyUsedListInit();

        this.materialPath.setText(UTIL_Runtime.APPLICATION_SETTINGS.getProperty("composer.materials"));
    }

    public void recentlyUsedListInit() {
        helper.fillRecentlyUsedStructuresTable();
        helper.initDoubleClickOnRecentluUsedTable();
    }

    public void createNewStructure(MouseEvent mouseEvent) {
        helper.openStructureEditor();
    }

    public void openStructureDefinitionFile(MouseEvent mouseEvent) {
        Structure newStructure = UTIL_StructureComposer.loadStructureFromFile();
        if(newStructure == null) return;
        UTIL_Runtime.MODEL_STRUCTURE = newStructure;

        helper.openStructureEditor();
    }

    public void mi_openAboutEbd(ActionEvent actionEvent) {
        UTIL_Methods.openAboutAppModal();
    }

    public void mi_quit(ActionEvent actionEvent) {
        System.exit(0);
    }
}
