package controllers;

import ebd.Calculate;
import engine.DiagramEngineController;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import org.apache.log4j.Logger;

public class ProgressDialogElementController {
    private static final Logger LOGGER = Logger.getLogger(ProgressDialogElementController.class.getName());
    public @FXML
    GridPane enginesGrid;
    public @FXML
    Label actionName;
    public @FXML
    Label percentLabel;
    public @FXML
    ProgressBar progressBar;

    public Calculate calculate;
    public ProgressDialogController parentController;

    public ProgressDialogElementController(ProgressDialogController parentController, Calculate calculate) {
        LOGGER.trace("Initializing ProgressDialogElementController...");
        this.calculate = calculate;
        this.parentController = parentController;
    }

    public void initialize() {
        LOGGER.trace("Inside ProgressDialogElementController initialize()...");
        this.progressBar.setPrefWidth(Double.MAX_VALUE);
        this.progressBar.setProgress(0);
        this.actionName.setText(calculate.getEngineController().getPropertyValue("engine.name"));
        this.percentLabel.setText(String.valueOf(calculate.getEngineController().progressProperty().get()));
        run();
    }

    public void run() {
        LOGGER.trace("Inside ProgressDialogElementController run()...");
        Task<Void> task = new Task<Void>() {
            @Override
            public Void call() throws InterruptedException, DiagramEngineController.DiagramEngineControllerException {
                calculate.getEngineController().progressProperty().addListener((obs, oldProgress, newProgress) -> {
                    System.out.println("PROGRESS VALUE CHANGED IN TASK!");
                    updateProgress(newProgress.doubleValue(), 1.0);
                });

                calculate.getEngineController().progressCommentProperty().addListener((obs, oldMessage, newMessage) -> {
                    System.out.println("PROGRESS MESSAGE CHANGED IN TASK!");
                    System.out.println(actionName.getText());
                    System.out.println(newMessage);
                    System.out.println(parentController);
                    parentController.logProgressInfo(actionName.getText(), newMessage);
                });

                Thread.sleep(2000);

                calculate.launchCalculation();
                updateProgress(1, 1);
                return null;
            }
        };

        this.activateProgressBar(task);

        task.setOnSucceeded(event -> {
            percentLabel.setText("100%");
            parentController.logProgressInfo(this.actionName.getText(), "[SUCCESS]");
            calculate.setRunning(false);
            this.parentController.enableCloseButton();
        });

        task.setOnFailed(event -> {
            percentLabel.setText("ERR");
            parentController.logProgressInfo(this.actionName.getText(), "[ERROR] " + task.getException().getMessage());
            calculate.setRunning(false);
            this.parentController.enableCloseButton();
        });

        Thread thread = new Thread(task);
        thread.start();
    }

    public void activateProgressBar(final Task<?> task) {
        progressBar.progressProperty().bind(task.progressProperty());
    }
}
