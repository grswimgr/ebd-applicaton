package controllers;

import ebd.main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Stage;
import model.Constant;
import org.gillius.jfxutils.chart.JFXChartUtil;
import utils.UTIL_Methods;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ApplicationController {
    @FXML LineChart testChart;
    @FXML NumberAxis X_axix;
    @FXML NumberAxis Y_axix;
    final double SCALE_DELTA = 1.1;
    private main main;

    ArrayList<XYChart.Data> chartAllData;
    ArrayList<XYChart.Data> dataToDisplay;

    Double sizeX;
    Double sizeY;

    Double zoomedSizeX, zoomedSizeY;
    Double zoomedStartFromX, zoomedStartFromY;

    Integer zoomFactor;

    public ApplicationController() {
        this.zoomFactor = 0;
    }

    public static HashMap<String, Constant> globalConstants = new HashMap<String, Constant>();

    // connect main class to controller
    public void setMain(main main) {
        this.main = main;
    }

    public void initialize() {
        System.out.println(testChart);
        this.chartAllData = generateSinusSampleData(100);
        this.dataToDisplay = this.chartAllData;

        this.X_axix.setLabel("Number of Month");
        this.Y_axix.setTickLabelFormatter(new NumberAxis.DefaultFormatter(Y_axix) {
            @Override
            public String toString(final Number object) {
                return String.format("%6.3e", object);
            }
        });
        //creating the chart
        this.testChart.setTitle("Stock Monitoring, 2010");
        //defining a series
        XYChart.Series series = new XYChart.Series();
        series.setName("My portfolio");
        //populating the series with data
        series.getData().addAll(this.dataToDisplay);

        this.sizeX = this.X_axix.getUpperBound();
        this.sizeY = this.Y_axix.getUpperBound();

        zoomedSizeX = sizeX;
        zoomedSizeY = sizeY;
        this.zoomedStartFromX = this.X_axix.getLowerBound();
        this.zoomedStartFromY = this.Y_axix.getLowerBound();

        this.testChart.getData().add(series);


        JFXChartUtil.setupZooming(this.testChart);
    }

    public void createNewModelWindow(ActionEvent actionEvent) {
        Parent root;
            try {

                root = FXMLLoader.load(getClass().getClassLoader().getResource("/view/HelloWorld.fxml"));
                Stage stage = new Stage();
                stage.setTitle("My New Stage Title");
                stage.setScene(new Scene(root, 450, 450));
                stage.show();

            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }


    public static HashMap<String, Constant> getGlobalConstants() {
        return globalConstants;
    }

    public static void setGlobalConstants(HashMap<String, Constant> globalConstants) {
        ApplicationController.globalConstants = globalConstants;
    }

    public void zoomChart(ScrollEvent scrollEvent) {
//        System.out.println("Handle event scroll :)...");
//        scrollEvent.consume();
//
//        if (scrollEvent.getDeltaY() == 0) {
//            return;
//        }
//
//        this.X_axix.setAutoRanging(false);
//        this.Y_axix.setAutoRanging(false);
//
//        System.out.println(scrollEvent.getDeltaX() + "  " + scrollEvent.getDeltaY());
    }

    public void handleMousePressed(MouseEvent mouseEvent) {
        System.out.println("Handle event click...");
        JFXChartUtil.addDoublePrimaryClickAutoRangeHandler(this.testChart);
    }

    public ArrayList<XYChart.Data> generateSinusSampleData(Integer pointsNumber) {
        ArrayList<XYChart.Data> result = new ArrayList<XYChart.Data>();
        for(Integer i = 1; i <= pointsNumber; i++) {
            Float xValue = Float.valueOf(i) / 10;
            result.add(new XYChart.Data(xValue, Math.sin(xValue)));
        }
        return result;
    }

    public void chartZoomPlus(MouseEvent mouseEvent) {
        zoomFactor += 2;
        this.zoomedSizeX = this.sizeX / this.zoomFactor;
        this.zoomedSizeY = this.sizeY / this.zoomFactor;
        this.zoomedStartFromX /= 2;
        this.zoomedStartFromY /= 2;
        //refreshViev();
    }

    public void chartZoomMinus(MouseEvent mouseEvent) {
        if(this.zoomFactor > 0) {
            this.zoomFactor -= 2;
            this.zoomedSizeX = this.sizeX * this.zoomFactor;
            this.zoomedSizeY = this.sizeY * this.zoomFactor;
            this.zoomedStartFromX *= 2;
            this.zoomedStartFromY *= 2;
        }
        //refreshViev();
    }

    public void refreshViev() {
        if(zoomFactor == 0) {
            this.dataToDisplay = this.chartAllData;
            this.X_axix.setAutoRanging(true);
            this.Y_axix.setAutoRanging(true);
        } else {
            this.X_axix.setAutoRanging(false);
            this.Y_axix.setAutoRanging(false);

            this.X_axix.setLowerBound(this.zoomedStartFromX);
            this.Y_axix.setLowerBound(this.zoomedStartFromX);
            this.X_axix.setUpperBound(this.zoomedStartFromX + this.zoomedSizeX);
            this.Y_axix.setUpperBound(this.zoomedStartFromY + this.zoomedSizeX);
        }
    }

    public void pickModelFile(MouseEvent mouseEvent) {
    }

    public void mi_openAboutEbd(ActionEvent actionEvent) {
        UTIL_Methods.openAboutAppModal();
    }

    public void mi_quit(ActionEvent actionEvent) {
        System.exit(0);
    }
}
