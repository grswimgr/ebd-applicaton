package controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Parameter;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;
import java.io.IOException;


public class ParameterListElementController extends ListCell<Parameter> {
    private static final Logger LOGGER = Logger.getLogger( ParameterListElementController.class.getName() );
    public HBox placeholder = new HBox();

    public Pane namePane = new Pane();
    public Label nameLabel = new Label();
    public Label valueLabel = new Label();
    public Label unitLabel = new Label();

    public Pane buttonsPane = new Pane();
    public HBox buttonsHbox = new HBox();
    public Button editButton = new Button();
    public Button deleteButton = new Button();

    public VBox labelsVBox = new VBox();
    public HBox labelsDetails = new HBox();

    ObservableList<Parameter> itemList;
    Boolean isMaterialEditor;

    public ParameterListElementController(ObservableList<Parameter> itemList, Boolean isMaterialEditor) {
        super();
        this.itemList = itemList;
        this.isMaterialEditor = isMaterialEditor;

        initComponentStructure();
        setupButtonsHandlers();
    }

    public void initComponentStructure() {
        this.placeholder.setPadding(new Insets(2));

        this.nameLabel.setPadding(new Insets(0, 20, 0, 0));
        this.valueLabel.setPadding(new Insets(5, 0, 0, 0));
        this.valueLabel.setFont(Font.font(9));
        this.unitLabel.setPadding(new Insets(5, 0, 0, 5));
        this.unitLabel.setFont(Font.font(9));

        this.editButton.setText("Edit");
        this.editButton.setPadding(new Insets(5));
        this.deleteButton.setText("Delete");
        this.deleteButton.setPadding(new Insets(5));
        this.buttonsHbox.getChildren().addAll(editButton, deleteButton);
        this.labelsDetails.getChildren().addAll(valueLabel, unitLabel);
        this.namePane.getChildren().addAll(labelsVBox);
        this.labelsVBox.getChildren().addAll(nameLabel, labelsDetails);
        this.buttonsPane.getChildren().addAll(buttonsHbox);
        this.placeholder.getChildren().addAll(namePane, buttonsPane);
        HBox.setHgrow(namePane, Priority.ALWAYS);
    }

    public void setupButtonsHandlers() {
        editButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Parameter param = (Parameter) getItem();
                System.out.println("Edit action for: " + param.getToken());
                openParameterEditor(param);
            }
        });

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Parameter param = (Parameter) getItem();
                System.out.println("Delete action for: " + param.getToken());
                itemList.remove(param);
            }
        });
    }

    @Override
    protected void updateItem(Parameter parameter, boolean empty) {
        super.updateItem(parameter, empty);
        if (!empty && parameter != null) {
            nameLabel.setText(parameter.getName());
            valueLabel.setText(parameter.getValueString());
            unitLabel.setText("(" + parameter.getUnit() + ")");
            setText(null);
            setGraphic(placeholder);
        } else {
            setText(null);
        }
    }

    public void openParameterEditor(Parameter parameter) {
        FXMLLoader root;
        VBox pane;
        try {
            if(isMaterialEditor) {
                root = new FXMLLoader(getClass().getResource("/view/materialParameter.fxml"));
                pane = root.load();
                MaterialParameterController mpc = root.getController();
                mpc.initData(itemList, parameter);
            } else {
                root = new FXMLLoader(getClass().getResource("/view/layerParameter.fxml"));
                pane = root.load();
                LayerParameterController lpc = root.getController();
                lpc.initData(itemList, parameter);
            }
            Stage secondStage  = new Stage();
            secondStage.setScene(new Scene(pane));
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.show();
        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Parameter Scene", e);
        }
    }
}
