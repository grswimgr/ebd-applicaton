package controllers;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Material;
import model.Parameter;
import org.apache.log4j.Logger;
import utils.UTIL;
import utils.UTIL_Methods;
import utils.UTIL_Runtime;

import java.io.IOException;

public class MaterialPropertiesController {
    private static final Logger LOGGER = Logger.getLogger(MaterialPropertiesController.class.getName());

    public ComboBox mpMaterialType;
    public TextField mpName;
    public TextArea mpDescription;
    public ListView mpParametersList;

    private StructureComposerController scc;
    private Material material;
    private Boolean isNewMaterial;
    private ObservableList<Parameter> itemList;

    public void initData(Material material, StructureComposerController scc) {
        if (material == null) {
            material = new Material();
            isNewMaterial = true;
        } else {
            isNewMaterial = false;
        }

        this.material = material;
        this.scc = scc;

        initMaterialTypePicklist();
        initParametersList();
        initLayerParametersList();
        mapMaterialToWindowFields();
    }

    public void initLayerParametersList() {
        itemList = FXCollections.observableArrayList(material.getParameters().values());

        itemList.addListener(new ListChangeListener<Parameter>() {
            public void onChanged(Change<? extends Parameter> c) {
                LOGGER.trace("Parameter List change detected!  Refreshing Viev...");
                while (c.next()) {
                    material.getParameters().clear();
                    for (Parameter permutatedItem : c.getList()) {
                        material.getParameters().put(permutatedItem.getToken(), permutatedItem);
                    }
                    refreshLayerParametersList();
                }
            }
        });

        this.mpParametersList.setCellFactory(property -> new ParameterListElementController(itemList, true));
        this.mpParametersList.setItems(itemList);
        this.mpParametersList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    //Use ListView's getSelected Item
                    Object currentItemSelected = mpParametersList.getSelectionModel().getSelectedItem();
                    new ParameterListElementController(itemList, true).openParameterEditor((Parameter) currentItemSelected);
                }
            }
        });
    }

    public void mapMaterialToWindowFields() {
        LOGGER.trace("Handle: mapMaterialToWindowFields");
        mpName.setText(this.getMaterial().getName());
        mpDescription.setText(this.getMaterial().getDescription());
        mpMaterialType.getSelectionModel().select(this.getMaterial().getType());

        refreshLayerParametersList();
    }


    public void mapWindowToMaterialFields() {
        LOGGER.trace("Handle: mapWindowToMaterialFields");
        this.getMaterial().setName(this.mpName.getText());
        this.getMaterial().setDescription(this.mpDescription.getText());
        this.getMaterial().setType((UTIL.MaterialType)this.mpMaterialType.getSelectionModel().getSelectedItem());
    }

    public void initMaterialTypePicklist() {
        mpMaterialType.setItems(
                FXCollections.observableArrayList(UTIL.MaterialType.values())
        );
    }

    public void initParametersList() {
        mpParametersList.getItems().clear();
        mpParametersList.setItems(FXCollections.observableArrayList(this.getMaterial().getParameters().values()));
        mpParametersList.setCellFactory(param -> new ListCell<Parameter>() {
            @Override
            protected void updateItem(Parameter item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getName() == null) {
                    setText(null);
                } else {
                    setText(item.getName());
                }
            }
        });
    }

    public void refreshLayerParametersList() {
        initLayerParametersList();
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public void addParameterHandler(MouseEvent mouseEvent) {
        addParameterHandler(new Parameter(), true);
    }

    public void addParameterHandler(Parameter parameter, Boolean newParameter) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/materialParameter.fxml"));
            VBox pane = root.load();
            MaterialParameterController mpc = root.getController();
            if(newParameter) {
                mpc.initData(itemList, null);
            } else {
                mpc.initData(itemList, parameter);
            }
            Stage secondStage  = new Stage();
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.setScene(new Scene(pane));
            secondStage.show();
        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Parameter Scene", e);
        }
    }

    public void saveMaterialHandler(MouseEvent mouseEvent) {
        LOGGER.trace("Handle: saveMaterial");

        try {
            mapWindowToMaterialFields();

            if(isNewMaterial) {
                UTIL_Runtime.MATERIALS_MAP.put(this.material.getName(), this.material);
            }

            // Reload material tables view
            scc.helper.initMaterialTables();

            // Close modal
            Stage scene = (Stage) this.mpName.getScene().getWindow();
            scene.close();
            LOGGER.trace("Paramater saved: " + this.mpName.getText());
        } catch(Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot save material!", e);
        }
    }

    public Boolean getNewMaterial() {
        return isNewMaterial;
    }

    public void cancelHandler(MouseEvent mouseEvent) {
        Stage scene = (Stage) this.mpName.getScene().getWindow();
        scene.close();
    }
}
