package controllers;

import engine.DiagramEngineController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;

import java.io.IOException;
import java.util.Properties;

public class SimulationEngineListElementController extends ListCell<DiagramEngineController> {
    private static final Logger LOGGER = Logger.getLogger( SimulationEngineListElementController.class.getName() );
    public @FXML HBox placeholder;
    public @FXML Label engineAuthor;
    public @FXML Label engineVersion;
    public @FXML Label engineName;
    public @FXML CheckBox isEngineActive;

    public void initialize() {
        LOGGER.trace("Initializing SimulationEngineListElementController...");
    }

    @Override
    protected void updateItem(DiagramEngineController controller, boolean empty) {
        super.updateItem(controller, empty);
        if (!empty && controller != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/simulationEngineListElement.fxml"));
            loader.setController(this);
            try {
                loader.load();
                engineName.setText(controller.getPropertyValue("engine.name"));
                engineVersion.setText(controller.getPropertyValue("engine.version"));
                engineAuthor.setText(controller.getPropertyValue("engine.author"));
                isEngineActive.setSelected(controller.isEnabled());

                isEngineActive.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        controller.setEnabled(newValue);
                    }
                });

                setText(null);
                setGraphic(placeholder);
            } catch (IOException e) {
                UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.WARNING, "Cannot load engine list!", e);
            }
        } else {
            setText(null);
        }
    }
}
