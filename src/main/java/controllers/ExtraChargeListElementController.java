package controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.ExtraCharge;
import model.Layer;
import model.Parameter;
import model.Structure;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;

import java.io.IOException;


public class ExtraChargeListElementController extends ListCell<ExtraCharge> {
    private static final Logger LOGGER = Logger.getLogger( ExtraChargeListElementController.class.getName() );
    public HBox placeholder = new HBox();

    public Pane detailsPane = new Pane();
    public Label positionLabel = new Label();
    public Label valueLabel = new Label();

    public Pane buttonsPane = new Pane();
    public HBox buttonsHbox = new HBox();
    public Button editButton = new Button();
    public Button deleteButton = new Button();

    public HBox labelsHBox = new HBox();

    ObservableList<ExtraCharge> itemList;
    Structure structure;
    Layer currentLayer;

    public ExtraChargeListElementController(ObservableList<ExtraCharge> itemList, Structure structure, Layer currentLayer) {
        super();
        this.itemList = itemList;
        this.structure = structure;
        this.currentLayer = currentLayer;

        initComponentStructure();
        setupButtonsHandlers();
    }

    public void initComponentStructure() {
        this.placeholder.setPadding(new Insets(2));

        this.positionLabel.setPadding(new Insets(0, 100, 0, 0));
        this.valueLabel.setPadding(new Insets(0, 0, 0, 0));

        this.editButton.setText("Edit");
        this.editButton.setPadding(new Insets(5));
        this.deleteButton.setText("Delete");
        this.deleteButton.setPadding(new Insets(5));
        this.buttonsHbox.getChildren().addAll(editButton, deleteButton);

        this.detailsPane.getChildren().addAll(labelsHBox);
        this.labelsHBox.getChildren().addAll(positionLabel, valueLabel);
        this.buttonsPane.getChildren().addAll(buttonsHbox);

        this.placeholder.getChildren().addAll(detailsPane, buttonsPane);
        HBox.setHgrow(detailsPane, Priority.ALWAYS);
    }

    public void setupButtonsHandlers() {
        editButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ExtraCharge charge = (ExtraCharge) getItem();
                openParameterEditor(charge);
            }
        });

        deleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ExtraCharge param = (ExtraCharge) getItem();
                itemList.remove(param);
            }
        });
    }

    @Override
    protected void updateItem(ExtraCharge charge, boolean empty) {
        super.updateItem(charge, empty);
        if (!empty && charge != null) {
            positionLabel.setText(String.valueOf(Double.valueOf(charge.getPosition() + LayerExtraChargeController.getCurrentLayerLocation(structure,currentLayer) * 1e9).intValue() + " (nm)"));
            valueLabel.setText(String.valueOf(charge.getChargeValue()) + " (e/cm2)");
            setText(null);
            setGraphic(placeholder);
        } else {
            setText(null);
        }
    }

    public void openParameterEditor(ExtraCharge extraCharge) {
        FXMLLoader root;
        VBox pane;
        try {
            root = new FXMLLoader(getClass().getResource("/view/layerExtraCharge.fxml"));
            pane = root.load();
            LayerExtraChargeController lec = root.getController();
            lec.initData(itemList, extraCharge, structure, currentLayer);

            Stage secondStage  = new Stage();
            secondStage.setScene(new Scene(pane));
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.show();
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Parameter Scene", e);
        }
    }
}
