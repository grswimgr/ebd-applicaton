package controllers;

import ebd.Calculate;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ProgressDialogController {
    private static final Logger LOGGER = Logger.getLogger( ProgressDialogController.class.getName() );

    public @FXML Pane mainProgressPane;
    public @FXML TextArea progressMessages;
    public @FXML Button closeButton;

    Stage stageToFreeze;
    List<Calculate> calculateList;

    public void initialize() {
        LOGGER.trace("Initializing ProgressDialogController...");
    }

    public void prepareCalculation(Stage stageToFreeze, List<Calculate> calculateList) {
        this.calculateList = calculateList;
        this.stageToFreeze = stageToFreeze;

        generateTasks();
    }

    private void generateTasks() {
        for(Calculate calc : calculateList) {
            FXMLLoader loader;
            try {
                loader = new FXMLLoader(getClass().getResource("/view/progressDialogElement.fxml"));
                ProgressDialogElementController controller = new ProgressDialogElementController(this, calc);
                loader.setController(controller);
                GridPane pane = loader.load();

                this.mainProgressPane.getChildren().add(pane);
            } catch (IOException e) {
                LOGGER.error(e);
                UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load progressDialogElement Scene", e);
            }
        }
    }

    public void logProgressInfo(String engineName, String message) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        String currentDateTime = now.format(dtf);

        Platform.runLater(() -> {
            String newMessage = String.format(String.format("\n%s [%s] %s",currentDateTime, engineName, message));
            LOGGER.debug(newMessage);
            String msg = this.progressMessages.getText() + newMessage;
            this.progressMessages.setText(msg);
        });
    }

    public void enableCloseButton() {
        for(Calculate calc : calculateList) {
            if(calc.getRunning()) return;
        }
        closeButton.setDisable(false);
    }

    public void closeWindow(MouseEvent mouseEvent) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}