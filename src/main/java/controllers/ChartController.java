package controllers;

//import com.github.javafx.charts.zooming.ZoomManager;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import org.gillius.jfxutils.chart.JFXChartUtil;

public class ChartController {

    SimulationController.LineChartWithMarkers chart;
    StackPane chartPane;

    ChartController(StackPane chartPane, SimulationController.LineChartWithMarkers chart) {
        this.chart = chart;
        this.chartPane = chartPane;
        setupChartControllers();
    }

    public void setupChartControllers() {
        JFXChartUtil.setupZooming(this.chart);
        this.chart.onMouseReleasedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                refreshLabelMarks();
            }
        });
        refreshLabelMarks();
    }

    public void resetZoom() {
        chart.getXAxis().setAutoRanging(true);
        chart.getYAxis().setAutoRanging(true);
        refreshLabelMarks();
    }

    public void zoomIn() {
        chart.getXAxis().setAutoRanging(false);
        chart.getYAxis().setAutoRanging(false);
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        NumberAxis yAxis = (NumberAxis) chart.getYAxis();

        Double xDelta = xAxis.getUpperBound() - xAxis.getLowerBound();
        Double yDelta = yAxis.getUpperBound() - yAxis.getLowerBound();

        xAxis.setLowerBound(xAxis.getLowerBound() + xDelta / 4);
        xAxis.setUpperBound(xAxis.getUpperBound() - xDelta / 4);
        yAxis.setLowerBound(yAxis.getLowerBound() + yDelta / 4);
        yAxis.setUpperBound(yAxis.getUpperBound() - yDelta / 4);

        refreshLabelMarks();
    }

    public void zoomOut() {
        chart.getXAxis().setAutoRanging(false);
        chart.getYAxis().setAutoRanging(false);
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        NumberAxis yAxis = (NumberAxis) chart.getYAxis();

        Double xDelta = xAxis.getUpperBound() - xAxis.getLowerBound();
        Double yDelta = yAxis.getUpperBound() - yAxis.getLowerBound();

        xAxis.setLowerBound(xAxis.getLowerBound() - xDelta / 4);
        xAxis.setUpperBound(xAxis.getUpperBound() + xDelta / 4);
        yAxis.setLowerBound(yAxis.getLowerBound() - yDelta / 4);
        yAxis.setUpperBound(yAxis.getUpperBound() + yDelta / 4);

        refreshLabelMarks();
    }

    public void clearAllData() {
        this.chart.removeAllVerticalMarkers();
        this.chart.removeAllHorizontalMarkers();
        this.chart.getData().clear();
    }

    public void refreshLabelMarks() {
        NumberAxis xAxis = (NumberAxis) chart.getXAxis();
        NumberAxis yAxis = (NumberAxis) chart.getYAxis();
        yAxis.setTickUnit(0.5);
        yAxis.setMinorTickCount(10);
        yAxis.setTickLabelsVisible(true);
        yAxis.setMinorTickVisible(true);

        xAxis.setTickUnit(1);
        xAxis.setMinorTickCount(10);
        xAxis.setTickLabelsVisible(true);
        xAxis.setMinorTickVisible(true);
    }
}
