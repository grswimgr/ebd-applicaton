package controllers;

import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Parameter;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;

public class MaterialParameterController {
    private static final Logger LOGGER = Logger.getLogger(MaterialParameterController.class.getName());

    public TextField mpName;
    public TextField mpSymbol;
    public TextField mpUnit;
    public TextField mpToken;
    public TextField mpValue;
    public CheckBox mpIsEditable;
    public CheckBox mpIsExpression;

    private MaterialPropertiesController mpc;
    private Parameter parameter;
    private Boolean isNewParameter;
    ObservableList<Parameter> parametersList;

    public void initData(ObservableList<Parameter> parametersList, Parameter parameter) {
        if (parameter == null) {
            parameter = new Parameter();
            isNewParameter = true;
        } else {
            isNewParameter = false;
        }
        this.parameter = parameter;
        this.parametersList = parametersList;
        this.mapParameterToWindowFields();
    }

    public void mapParameterToWindowFields() {
        mpName.setText(this.parameter.getName());
        mpSymbol.setText(this.parameter.getSymbol());
        mpUnit.setText(this.parameter.getUnit());
        mpToken.setText(this.parameter.getToken());
        mpIsEditable.setSelected(this.parameter.getEditable());
        mpIsExpression.setSelected(this.parameter.getExpression());

        if(this.parameter.getExpression()) {
            mpValue.setText(this.parameter.getValueString());
        } else {
            mpValue.setText(String.valueOf(this.parameter.getValue()));
        }
    }

    public void mapWindowFieldsToParameter() {
        this.parameter.setName(this.mpName.getText());
        this.parameter.setSymbol(this.mpSymbol.getText());
        this.parameter.setUnit(this.mpUnit.getText());
        this.parameter.setToken(this.mpToken.getText());
        this.parameter.setEditable(this.mpToken.isEditable());
        this.parameter.setEditable(this.mpIsEditable.isSelected());
        this.parameter.setExpression(this.mpIsExpression.isSelected());
        this.parameter.setValue(this.mpValue.getText());
    }

    public void saveMaterialParameterHandler(MouseEvent mouseEvent) {
        try {
            Integer oldParameterIndex = parametersList.indexOf(parameter);

            mapWindowFieldsToParameter();

            // Save parameter if new
            if(isNewParameter) {
                this.parametersList.add(parameter);
            } else {
                this.parametersList.remove(oldParameterIndex);
                this.parametersList.add(parameter);
            }

            // Close modal
            Stage scene = (Stage) this.mpName.getScene().getWindow();
            scene.close();
            LOGGER.trace("Paramater saved: " + this.mpName.getText());
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot save parameter", e);
        }
    }

    public void cancelHandler(MouseEvent mouseEvent) {
        Stage scene = (Stage) this.mpName.getScene().getWindow();
        scene.close();
    }
}
