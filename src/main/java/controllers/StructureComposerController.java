package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import helpers.StructureComposerHelper;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import javafx.util.StringConverter;
import model.*;
import org.apache.log4j.Logger;
import utils.*;


public class StructureComposerController {
    private static final Logger LOGGER = Logger.getLogger( StructureComposerController.class.getName() );

    @FXML public TableView strCmpMetalTable;
    @FXML public TableView strCmpSemiconductorTable;
    @FXML public TableView strCmpDielectricTable;
    @FXML public TableView strCmpTable;
    
    @FXML public TextField strCmpName;
    @FXML public TextArea strCmpDescription;
    @FXML public StackedBarChart structureChart;
    @FXML public AnchorPane structureChartPane;

    public Map<String, TableView> tablesByNameMap;
    public ContextMenu materialRowMenu;
    public ContextMenu structureRowMenu;

    StructureComposerHelper helper;

    public void initialize() {
        LOGGER.info("StructureComposerController.initialize()");

        helper = new StructureComposerHelper(this);
        //CUSTOM_DIAGRAM_ENGINE_HANDLER.SetControllerParameters(UTIL_Runtime.MODEL_CONFIGURATION, UTIL_Runtime.MODEL_STRUCTURE);

        tablesByNameMap = new HashMap<String, TableView>(){};
        tablesByNameMap.put("strCmpMetalTable", strCmpMetalTable);
        tablesByNameMap.put("strCmpSemiconductorTable", strCmpSemiconductorTable);
        tablesByNameMap.put("strCmpDielectricTable", strCmpDielectricTable);
        tablesByNameMap.put("strCmpTable", strCmpTable);

        initStructure();
        initMaterialRowMenu();
        this.helper.initStructureRowMenu();
        initTableColumnsMapping();
        initStructurePreviewChart();
    }

    private void initStructurePreviewChart() {
        CategoryAxis xAxis = (CategoryAxis) this.structureChart.getXAxis();
        xAxis.setCategories(FXCollections.<String>observableArrayList(Arrays.asList
                (this.getStructure().getName())));

        xAxis.setLabel("Layer");
        NumberAxis yAxis = (NumberAxis) this.structureChart.getYAxis();
        yAxis.setLabel("Structure thickness");

        NumberFormat format = new DecimalFormat("#.#E0");
        yAxis.setTickLabelFormatter(new StringConverter<Number>() {
            @Override
            public String toString(Number number) {
                return format.format(number.doubleValue());
            }

            @Override
            public Number fromString(String string) {
                try {
                    return format.parse(string);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0 ;
                }
            }
        });

        this.helper.refreshStructureChart();

    }

    public void initData() {
        helper.initMaterialTables();

        this.strCmpTable.getItems().clear();
        this.strCmpTable.setItems(FXCollections.observableArrayList(UTIL_Runtime.MODEL_STRUCTURE.getLayers()));
    }

    public void refreshMaterialTables() {

    }

    public void handleMetalTableClick(MouseEvent mouseEvent) {
        System.out.println("Handle click metal" + mouseEvent.getButton());
        //handleTableRightClick(mouseEvent, this.strCmpMetalTable);
    }

    public void handleTableRightClick(MouseEvent evt, TableView table, Object rowItem, ContextMenu menu) {
        if(evt.getButton().name() != "SECONDARY" || rowItem == null) {
            this.materialRowMenu.hide();
            this.structureRowMenu.hide();
            return;
        }

        menu.show(table, evt.getScreenX(), evt.getScreenY());
    }

    public void initMaterialRowMenu() {
        this.strCmpMetalTable.setRowFactory( tv -> {
            TableRow<Material> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                handleTableRightClick(event, this.strCmpMetalTable, row.getItem(), materialRowMenu);

                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    helper.openMaterialEditor((Material) this.strCmpMetalTable.getItems().get(this.strCmpMetalTable.getSelectionModel().getFocusedIndex()));
                }
            });

            return row ;
        });

        this.strCmpDielectricTable.setRowFactory( tv -> {
            TableRow<Material> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                handleTableRightClick(event, this.strCmpDielectricTable, row.getItem(), materialRowMenu);

                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    helper.openMaterialEditor((Material) this.strCmpDielectricTable.getItems().get(this.strCmpDielectricTable.getSelectionModel().getFocusedIndex()));
                }
            });

            return row ;
        });

        this.strCmpSemiconductorTable.setRowFactory( tv -> {
            TableRow<Material> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                handleTableRightClick(event, this.strCmpSemiconductorTable, row.getItem(), materialRowMenu);

                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    helper.openMaterialEditor((Material) this.strCmpSemiconductorTable.getItems().get(this.strCmpSemiconductorTable.getSelectionModel().getFocusedIndex()));
                }
            });

            return row ;
        });

        this.materialRowMenu = new ContextMenu();
        MenuItem editItem = new MenuItem("Edit");
        editItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Handle edit for: " + materialRowMenu.getOwnerNode().getId());
                TableView tw = getTablesByNameMap().get(materialRowMenu.getOwnerNode().getId());
                System.out.println(tw.getSelectionModel().getFocusedIndex());

                helper.openMaterialEditor((Material) tw.getItems().get(tw.getSelectionModel().getFocusedIndex()));
                //table.getItems().remove(row.getItem());
            }
        });

        MenuItem addToStructure = new MenuItem("Add to structure");
        addToStructure.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Handle edit for: " + materialRowMenu.getOwnerNode().getId());
                TableView tw = getTablesByNameMap().get(materialRowMenu.getOwnerNode().getId());
                Integer focusedIndex = tw.getSelectionModel().getFocusedIndex();
                System.out.println(focusedIndex);
                Layer layer = new Layer((Material)tw.getItems().get(focusedIndex));
                UTIL_Runtime.MODEL_STRUCTURE.getLayers().add(layer);
                openLayerPropertiesWindow(layer);

            }
        });

        this.materialRowMenu.getItems().addAll(editItem, addToStructure);
    }

    public void openLayerPropertiesWindow(Layer layer) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/layerProperties.fxml"));
            VBox pane = root.load();
            //VBox pane = root.load();
            LayerPropertiesController lpc = root.getController();
            lpc.initData(layer, this);

            Stage stage = new Stage();
            stage.setTitle("Layer Properties Editor");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(new Scene(pane));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initStructure() {
        if(UTIL_Runtime.MODEL_STRUCTURE == null) {
            UTIL_Runtime.MODEL_STRUCTURE = new Structure();
            UTIL_Runtime.MODEL_STRUCTURE.setName("Sample Structure name");
            UTIL_Runtime.MODEL_STRUCTURE.setDescription("Sample Description");
            UTIL_Runtime.MODEL_STRUCTURE.setLayers(new ArrayList<Layer>());
        } else {
            this.helper.refreshCmpTable();
            this.refreshCmpFields();
            this.helper.refreshStructureChart();
            initData();
        }
    }

    public void initTableColumnsMapping() {
        helper.setMetalColumns();
        helper.setDielectricColumns();
        helper.setSemiconductorColumns();
        helper.setStructureColumns();
    }

    public void refreshCmpFields() {
        this.strCmpName.setText(UTIL_Runtime.MODEL_STRUCTURE.getName());
        this.strCmpDescription.setText(UTIL_Runtime.MODEL_STRUCTURE.getDescription());
    }

    public Map<String, TableView> getTablesByNameMap() {
        return tablesByNameMap;
    }

    public void setTablesByNameMap(Map<String, TableView> tablesByNameMap) {
        this.tablesByNameMap = tablesByNameMap;
    }

    public ContextMenu getTableRowMenu() {
        return materialRowMenu;
    }

    public void setTableRowMenu(ContextMenu tableRowMenu) {
        this.materialRowMenu = tableRowMenu;
    }

    public Structure getStructure() {
        return UTIL_Runtime.MODEL_STRUCTURE;
    }

    public void setStructure(Structure structure) {
        UTIL_Runtime.MODEL_STRUCTURE = structure;
    }

    public void saveStructureHandler(ActionEvent actionEvent) {
        mapWinndowToModelFields();
        UTIL_StructureComposer.saveStructureJSON(UTIL_Runtime.MODEL_STRUCTURE);
    }

    public void mapWinndowToModelFields() {
        this.getStructure().setName(this.strCmpName.getText());
        this.getStructure().setDescription(this.strCmpDescription.getText());
    }

    public void loadStructureHandler(ActionEvent actionEvent) {
        Structure newStructure = UTIL_StructureComposer.loadStructureFromFile();
        if(newStructure == null) return;
        UTIL_Runtime.MODEL_STRUCTURE = newStructure;
        this.helper.refreshCmpTable();
        this.refreshCmpFields();
        this.helper.refreshStructureChart();
        System.out.println("Structure readed successfully!");
    }

    public void startSimulationHandler(ActionEvent actionEvent) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/simulationWindowV2.fxml"));
            VBox pane = root.load();

            UTIL_Runtime.SIMULATION_SCENE = new Scene(pane);
            UTIL_Runtime.MAIN_WINDOW_STAGE.setScene(UTIL_Runtime.SIMULATION_SCENE);

        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Simulation Scene", e);
        }
    }

    public void saveMaterials(ActionEvent mouseEvent) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String filePath = UTIL_Runtime.APPLICATION_SETTINGS.getProperty("composer.materials");

            String materialsToSave = objectMapper.writeValueAsString(UTIL_Runtime.MATERIALS_MAP);

            UTIL_DataTransfer.saveFileContent(filePath, materialsToSave);
            UTIL_Methods.showPopup(Alert.AlertType.INFORMATION, "File saved Successfully!", "Path: " + filePath);
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot save Materials", e);
        }
    }

    public void createNewMaterialHandler(ActionEvent actionEvent) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/materialProperties.fxml"));
            VBox pane = root.load();
            MaterialPropertiesController mpc = root.getController();
            mpc.initData(null, this);
            Stage secondStage  = new Stage();
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.setScene(new Scene(pane));
            secondStage.show();
        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Properties Scene", e);
        }
    }

    public void mi_openAboutEbd(ActionEvent actionEvent) {
        UTIL_Methods.openAboutAppModal();
    }

    public void mi_quit(ActionEvent actionEvent) {
        System.exit(0);
    }
}
