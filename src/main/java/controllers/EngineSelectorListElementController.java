package controllers;

import helpers.SelectEngineWindowHelper;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Properties;

public class EngineSelectorListElementController extends ListCell<Properties> {
    private static final Logger LOGGER = Logger.getLogger( EngineSelectorListElementController.class.getName() );
    public @FXML VBox placeholder;
    public @FXML Label engineLocation;
    public @FXML Label engineAuthor;
    public @FXML Label engineVersion;
    public @FXML Label engineName;

    public void initialize() {
        LOGGER.trace("Initializing EngineSelectorListElementController...");
    }

    @Override
    protected void updateItem(Properties properties, boolean empty) {
        super.updateItem(properties, empty);
        if (!empty && properties != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/selectEngineListElement.fxml"));
            loader.setController(this);
            try {
                loader.load();
                engineName.setText( (properties.containsKey("engine.name")) ? properties.getProperty("engine.name") : "-");
                engineVersion.setText( (properties.containsKey("engine.version")) ? properties.getProperty("engine.version") : "-");
                engineAuthor.setText( (properties.containsKey("engine.author")) ? properties.getProperty("engine.author") : "-");
                engineLocation.setText( (properties.containsKey("engine.jar")) ? properties.getProperty("engine.jar") : "-");
                setText(null);
                setGraphic(placeholder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            setText(null);
        }
    }
}
