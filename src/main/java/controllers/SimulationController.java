package controllers;

import engine.DiagramEngineController;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.Axis;
import javafx.scene.image.WritableImage;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.shape.Line;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.*;
import org.apache.log4j.Logger;
import utils.*;
import helpers.SimulationWindowHelper;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SimulationController {
    private static final Logger LOGGER = Logger.getLogger(SimulationController.class.getName() );

    @FXML public ComboBox simulationTypePicklist;
    @FXML public Label modelNameLabel;
    @FXML public Label modelDescriptionLabel;
    @FXML public Label modelAuthorLabel;
    @FXML public Button chartZoomResetButton;
    @FXML public Button chartZoomMinusButton;
    @FXML public Button chartZoomPlusButton;
    @FXML public LineChartWithMarkers chart;
    @FXML public NumberAxis chartXAxis;
    @FXML public NumberAxis chartYAxis;
    @FXML public Label modelNameLabelBottom;
    @FXML public Label simulationStatusLabelBottom;
    @FXML public StackPane chartPane;
    @FXML public Button clearDataButton;
    @FXML public Label structureNameLabel;
    @FXML public Label structureDescriptionLabel;
    @FXML public Label layersNumberLabel;
    @FXML public Label totalThicknessLabel;
    @FXML public TreeTableView<Parameter> layersViewTree;
    @FXML public TreeView<Object> visibleSeriesTree;
    @FXML public Button saveParameterBtn;
    @FXML public TextField parameterValueTF;
    @FXML public TextField pointsSpacing;
    @FXML public ListView engineList;
    @FXML public VBox windowPlaceholder;
    @FXML public TextField temperature;
    @FXML public TextField voltage;
    @FXML public Label structureNameLabelBottom;

    ChartController chartController;
    SimulationWindowHelper helper;
    List<DiagramEngineController> diagramEngineControllers = new ArrayList<>();

    public void initialize() {
        helper = new SimulationWindowHelper(this);
        helper.loadEngineList();
        LOGGER.debug("ENGINES READY TO USE: " + UTIL_Runtime.ENGINE_CONTROLLER_HANDLERS.values().size());

        for(DiagramEngineController controller : UTIL_Runtime.ENGINE_CONTROLLER_HANDLERS.values()) {
            controller.setControllerParameters(UTIL_Runtime.MODEL_STRUCTURE);
        }

        loadParametersTree();
        loadSeriesTree();
        laodPicklistsValues();
        loadStructureInformation();
        initChart();
        initChartControll();

        helper.initSimulationModeChangeAction();
    }

    public void loadSeriesTree() {
        try {
            helper.updateSeriesTree();
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unexepected error!", e);
            e.printStackTrace();
            System.out.println("loadTreeView exception!");
        }
    }

    public void loadParametersTree() {
        try {
            helper.prepareParametersTree();
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unexepected error!", e);
            e.printStackTrace();
            System.out.println("loadTreeView exception!");
        }
    }

    public void initChart() {
        chartXAxis = new NumberAxis();
        chartXAxis.setLabel("Distance (nm)");
        chartYAxis = new NumberAxis();
        chartYAxis.setLabel("Energy (eV)");
        chart = new LineChartWithMarkers<Number,Number>(chartXAxis,chartYAxis);
        chartPane.getChildren().add(chart);

        NumberAxis yAxis = (NumberAxis) this.chart.getYAxis();
        NumberFormat format = new DecimalFormat("#.#E0");
        yAxis.setTickLabelFormatter(new StringConverter<Number>() {

            @Override
            public String toString(Number number) {
                return format.format(number.doubleValue());
            }

            @Override
            public Number fromString(String string) {
                try {
                    return format.parse(string);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return 0 ;
                }
            }
        });
    }

    public void initChartControll() {
        this.chartController = new ChartController(chartPane, chart);
    }

    public void laodPicklistsValues() {
        simulationTypePicklist.setItems(
                FXCollections.observableArrayList("Energy", "Potential", "Electric Field", "Charge Density")
        );
        simulationTypePicklist.getSelectionModel().selectFirst();
    }

    public void loadStructureInformation() {
        structureNameLabel.setText(UTIL_Runtime.MODEL_STRUCTURE.getName());
        structureDescriptionLabel.setText(UTIL_Runtime.MODEL_STRUCTURE.getDescription());
        layersNumberLabel.setText(String.valueOf(UTIL_Runtime.MODEL_STRUCTURE.getLayers().size()));
        structureNameLabelBottom.setText(UTIL_Runtime.MODEL_STRUCTURE.getName());
    }

    @FXML
    public void runSimulationHandler(MouseEvent mouseEvent) {
        helper.runSimulation();
    }

    @FXML
    public void chartZoomResetHandler(MouseEvent mouseEvent) {
        this.chartController.resetZoom();
    }

    @FXML
    public void chartZoomOutHandler(MouseEvent mouseEvent) {
        this.chartController.zoomOut();
    }

    @FXML
    public void chartZoomInHandler(MouseEvent mouseEvent) {
        this.chartController.zoomIn();
    }

    @FXML
    public void clearDataHandler(MouseEvent mouseEvent) {
        this.chartController.clearAllData();
        this.visibleSeriesTree.setRoot(null);

        for(DiagramEngineController dac : getDiagramEngineControllers()) {
            dac.setCalculationResults(new ArrayList<>());
        }

        this.getHelper().updateSeriesTree();
    }

    @FXML
    public void mi_openStructureHandler(ActionEvent actionEvent) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/structureComposer.fxml"));
            VBox pane = root.load();

            if(UTIL_Runtime.STRUCTURE_COMPOSER_SCENE == null) {
                UTIL_Runtime.STRUCTURE_COMPOSER_SCENE = new Scene(pane);
            }

            UTIL_Runtime.MAIN_WINDOW_STAGE.setScene(UTIL_Runtime.STRUCTURE_COMPOSER_SCENE);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void saveParameterValueChange(MouseEvent mouseEvent) {
        LOGGER.trace("Button saveParameterValueChange!");
        Double newValue = Double.valueOf(this.parameterValueTF.getText());
        Parameter param = this.layersViewTree.getFocusModel().getFocusedItem().getValue();
        param.setValue(newValue);
        param.setValueString(String.valueOf(newValue));

        this.saveParameterBtn.setDisable(true);

        // WORKAROUND JAVAFX REFRESH ISSUE
        TreeItem ti = this.layersViewTree.getFocusModel().getFocusedItem();
        ti.setValue(false);
        layersViewTree.refresh();
        ti.setValue(param);
        layersViewTree.refresh();
    }

    @FXML
    public void copyToClipboard(MouseEvent mouseEvent) {
        WritableImage image = chartPane.snapshot(new SnapshotParameters(), null);
        ClipboardContent cc = new ClipboardContent();
        cc.putImage(image);
        Clipboard.getSystemClipboard().setContent(cc);
    }

    @FXML
    public void exportToCsv(MouseEvent mouseEvent) {
        // Get PATH
        DirectoryChooser directoryChooser = new DirectoryChooser();
        Stage stage = new Stage();
        stage.setTitle("Data export - destination folder");
        File selectedDirectory = directoryChooser.showDialog(stage);

        if(directoryChooser == null) return;

        for(DiagramEngineController dec : diagramEngineControllers) {
            if(dec.isEnabled()) {
                UTIL_DataTransfer.exportResultsToCSV(selectedDirectory.getPath(), dec);
            }
        }
    }

    public SimulationWindowHelper getHelper() {
        return helper;
    }

    public List<DiagramEngineController> getDiagramEngineControllers() {
        return diagramEngineControllers;
    }


    public void refreshButtonHandler(ActionEvent actionEvent) {
        this.helper.updateChartSeries();
        this.helper.updateSeriesTree();
    }


    public void mi_openAboutEbd(ActionEvent actionEvent) {
        UTIL_Methods.openAboutAppModal();
    }

    public void mi_quit(ActionEvent actionEvent) {
        System.exit(0);
    }

    public class LineChartWithMarkers<X,Y> extends LineChart {

        private ObservableList<Data<X, Y>> horizontalMarkers;
        private ObservableList<Data<X, Y>> verticalMarkers;

        public LineChartWithMarkers(Axis<X> xAxis, Axis<Y> yAxis) {
            super(xAxis, yAxis);
            horizontalMarkers = FXCollections.observableArrayList(data -> new Observable[] {data.YValueProperty()});
            horizontalMarkers.addListener((InvalidationListener) observable -> layoutPlotChildren());
            verticalMarkers = FXCollections.observableArrayList(data -> new Observable[] {data.XValueProperty()});
            verticalMarkers.addListener((InvalidationListener)observable -> layoutPlotChildren());
        }

        public void addHorizontalValueMarker(List<Data<X, Y>> markers) {
            for(Data<X,Y> marker : markers) {
                addHorizontalValueMarker(marker);
            }
        }

        public void addHorizontalValueMarker(Data<X, Y> marker) {
            Objects.requireNonNull(marker, "the marker must not be null");
            if (horizontalMarkers.contains(marker)) return;
            Line line = new Line();
            marker.setNode(line );
            getPlotChildren().add(line);
            horizontalMarkers.add(marker);
        }

        public void removeHorizontalValueMarker(Data<X, Y> marker) {
            Objects.requireNonNull(marker, "the marker must not be null");
            if (marker.getNode() != null) {
                getPlotChildren().remove(marker.getNode());
                marker.setNode(null);
            }
            horizontalMarkers.remove(marker);
        }

        public void addVerticalValueMarker(List<Data<X, Y>> markers) {
            for(Data<X,Y> marker : markers) {
                addVerticalValueMarker(marker);
            }
        }

        public void addVerticalValueMarker(Data<X, Y> marker) {
            Objects.requireNonNull(marker, "the marker must not be null");
            if (verticalMarkers.contains(marker)) return;
            Line line = new Line();
            marker.setNode(line );
            getPlotChildren().add(line);
            verticalMarkers.add(marker);
        }

        public void removeVerticalValueMarker(Data<X, Y> marker) {
            Objects.requireNonNull(marker, "the marker must not be null");
            if (marker.getNode() != null) {
                getPlotChildren().remove(marker.getNode());
                marker.setNode(null);
            }
            verticalMarkers.remove(marker);
        }

        public void removeAllVerticalMarkers() {
            for(Data<X,Y> marker : (List<Data<X, Y>>) verticalMarkers) {
                Platform.runLater(() -> {
                    removeVerticalValueMarker(marker);
                });
            }
        }

        public void removeAllHorizontalMarkers() {
            for(Data<X,Y> marker : (List<Data<X, Y>>) horizontalMarkers) {
                Platform.runLater(() -> {
                    removeHorizontalValueMarker(marker);
                });
            }
        }

        @Override
        protected void layoutPlotChildren() {
            super.layoutPlotChildren();
            for (Data<X, Y> horizontalMarker : horizontalMarkers) {
                Line line = (Line) horizontalMarker.getNode();
                line.setStartX(0);
                line.setEndX(getBoundsInLocal().getWidth());
                line.setStartY(getYAxis().getDisplayPosition(horizontalMarker.getYValue()) + 0.5); // 0.5 for crispness
                line.setEndY(line.getStartY());
                line.toFront();
            }
            for (Data<X, Y> verticalMarker : verticalMarkers) {
                Line line = (Line) verticalMarker.getNode();
                line.setStartX(getXAxis().getDisplayPosition(verticalMarker.getXValue()) + 0.5);  // 0.5 for crispness
                line.setEndX(line.getStartX());
                line.setStartY(0d);
                line.setEndY(getBoundsInLocal().getHeight());
                line.toFront();
            }
        }

    }
}