package controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.ExtraCharge;
import model.Layer;
import model.Structure;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;

public class LayerExtraChargeController {
    private static final Logger LOGGER = Logger.getLogger(LayerExtraChargeController.class.getName());

    public static class ChargeDefinitionException extends Exception {
        public ChargeDefinitionException(String message) {
            super(message);
        }
    }

    @FXML public TextField chargePosition;
    @FXML public TextField chargeValue;
    @FXML public Label namePreviousLayer;
    @FXML public Label positionPreviousLayer;
    @FXML public Label nameCurrentLayer;
    @FXML public Slider chargeValueSlider;
    @FXML public Label nameNextLayer;
    @FXML public Label positionNextLayer;

    private ExtraCharge extraCharge;
    private Structure structure;
    private Layer currentLayer;
    private ObservableList<ExtraCharge> chargesList;
    private Boolean isNewCharge;

    public void initData(ObservableList<ExtraCharge> chargesList, ExtraCharge extraCharge, Structure structure, Layer currentLayer) throws ChargeDefinitionException {
        if (extraCharge == null) {
            extraCharge = new ExtraCharge(0.0,0.0);
            isNewCharge = true;
        } else {
            isNewCharge = false;
        }
        this.extraCharge = extraCharge;
        this.chargesList = chargesList;
        this.structure = structure;
        this.currentLayer = currentLayer;

        this.mapChargeToWindowFields();

        this.chargeValueSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            chargePosition.textProperty().setValue(String.valueOf(newValue.intValue()));
        });

        this.chargePosition.textProperty().addListener((observable, oldValue, newValue) -> {
            this.chargeValueSlider.setValue(Integer.valueOf(newValue));
        });
    }

    public void mapChargeToWindowFields() throws ChargeDefinitionException {
        this.chargePosition.setText(String.valueOf(Double.valueOf(extraCharge.getPosition()).intValue() + Double.valueOf(this.currentLayer.getThickness() * 1e9).intValue()));
        this.positionPreviousLayer.setText(String.valueOf(getCurrentLayerLocation() * 1e9) + " nm");
        this.chargeValueSlider.setMin(getCurrentLayerLocation() * 1e9);
        this.positionNextLayer.setText(String.valueOf(getCurrentLayerLocation() * 1e9 + this.currentLayer.getThickness() * 1e9) + " nm");
        this.chargeValueSlider.setMax(getCurrentLayerLocation() * 1e9 + this.currentLayer.getThickness() * 1e9);
        this.nameCurrentLayer.setText(this.currentLayer.getName());

        this.chargeValue.setText(String.valueOf(extraCharge.getChargeValue()));
        this.chargeValueSlider.setValue(Double.valueOf(extraCharge.getPosition()).intValue() + Double.valueOf(this.currentLayer.getThickness() * 1e9).intValue());
        this.chargeValueSlider.setMajorTickUnit(1);
        this.chargeValueSlider.setMinorTickCount(0);
        this.chargeValueSlider.setShowTickMarks(true);
        this.chargeValueSlider.setShowTickLabels(true);
        this.chargeValueSlider.setSnapToTicks(true);

        Integer currentLayerIndex = this.structure.getLayers().indexOf(this.currentLayer);
        if(currentLayerIndex != 0) {
            this.namePreviousLayer.setText(this.structure.getLayers().get(currentLayerIndex-1).getName());
            if(currentLayerIndex != this.structure.getLayers().size() - 1) {
                this.nameNextLayer.setText(this.structure.getLayers().get(currentLayerIndex + 1).getName());
            } else {
                throw new ChargeDefinitionException("Current layer cannot be the last one of the Structure! Please define the last layer as a Semiconductor!");
            }
        } else {
            throw new ChargeDefinitionException("Current layer cannot be the first one of the Structure! Please define first layer as a Metal!");
        }
    }

    public void mapWindowFieldsToCharge() {
        extraCharge.setPosition(Double.valueOf(this.chargePosition.getText()) - getCurrentLayerLocation() * 1e9);
        extraCharge.setChargeValue(Double.valueOf(this.chargeValue.getText()));
    }


    public void cancelHandler(MouseEvent mouseEvent) {
        Stage scene = (Stage) this.chargeValue.getScene().getWindow();
        scene.close();
    }

    public void saveExtraChargeHandler(MouseEvent mouseEvent) {
        try {
            Integer oldChargeIndex = chargesList.indexOf(extraCharge);

            mapWindowFieldsToCharge();

            // Save parameter if new
            if(isNewCharge) {
                this.chargesList.add(extraCharge);
            } else {
                this.chargesList.remove(oldChargeIndex);
                this.chargesList.add(extraCharge);
            }

            // Close modal
            Stage scene = (Stage) this.chargePosition.getScene().getWindow();
            scene.close();
            LOGGER.trace("Charge saved: " + this.chargeValue.getText());
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot save parameter", e);
        }
    }

    private Double getCurrentLayerLocation() {
        return getCurrentLayerLocation(this.structure, this.currentLayer);
    }

    public static Double getCurrentLayerLocation(Structure structure, Layer currentLayer) {
        Double result = 0.0;

        for(Layer layer : structure.getLayers()) {
            if(layer.equals(currentLayer)) {
                return result;
            } else {
                result += layer.getThickness();
            }
        }

        return result;
    }
}
