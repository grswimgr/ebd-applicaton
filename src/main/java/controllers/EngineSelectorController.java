package controllers;

import ebd.main;
import helpers.SelectEngineWindowHelper;
import helpers.SelectStructureWindowHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import model.Structure;
import org.apache.log4j.Logger;
import utils.UTIL_Runtime;
import utils.UTIL_StructureComposer;

public class EngineSelectorController {
    private static final Logger LOGGER = Logger.getLogger( EngineSelectorController.class.getName() );

    public @FXML ListView engineList;
    public @FXML Label engineNameLabel;
    public @FXML Label engineDescriptionLabel;
    public @FXML Label engineAuthorLabel;
    public @FXML Button launchSimulator;

    public SelectEngineWindowHelper helper;

    public void initialize() {
        LOGGER.debug("Initializing EngineSelectorController...");
        helper = new SelectEngineWindowHelper(this);
        helper.prepareEnginesList();
    }

    public void loadEngines(MouseEvent mouseEvent) {
        helper.loadAllEngines();
    }

    public void mi_openAboutEbd(ActionEvent actionEvent) {
    }
}
