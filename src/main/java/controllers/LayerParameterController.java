package controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Parameter;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;

public class LayerParameterController {
    private static final Logger LOGGER = Logger.getLogger(LayerParameterController.class.getName());

    @FXML public TextField lppName;
    @FXML public TextField lppSymbol;
    @FXML public TextField lppToken;
    @FXML public TextField lppUnit;
    @FXML public TextField lppValue;
    @FXML public CheckBox lppIsEditable;
    @FXML public CheckBox lppIsExpression;

    private Parameter parameter;

    ObservableList<Parameter> parametersList;
    Boolean isNewParameter;

    public void initData(ObservableList<Parameter> parametersList, Parameter parameter) {
        if (parameter == null) {
            parameter = new Parameter();
            isNewParameter = true;
        } else {
            isNewParameter = false;
        }
        this.parameter = parameter;
        this.parametersList = parametersList;
        this.mapParameterToWindowFields();
    }

    public void mapParameterToWindowFields() {
        lppName.setText(this.parameter.getName());
        lppSymbol.setText(this.parameter.getSymbol());
        lppUnit.setText(this.parameter.getUnit());
        lppToken.setText(this.parameter.getToken());
        lppIsEditable.setSelected(this.parameter.getEditable());
        lppIsExpression.setSelected(this.parameter.getExpression());

        if(this.parameter.getExpression()) {
            lppValue.setText(this.parameter.getValueString());
        } else {
            lppValue.setText(String.valueOf(this.parameter.getValue()));
        }
    }

    public void mapWindowFieldsToParameter() {
        this.parameter.setName(this.lppName.getText());
        this.parameter.setSymbol(this.lppSymbol.getText());
        this.parameter.setUnit(this.lppUnit.getText());
        this.parameter.setToken(this.lppToken.getText());
        this.parameter.setEditable(this.lppIsEditable.isSelected());
        this.parameter.setExpression(this.lppIsExpression.isSelected());
        this.parameter.setValue(this.lppValue.getText());
    }


    public void cancelHandler(MouseEvent mouseEvent) {
        Stage scene = (Stage) this.lppName.getScene().getWindow();
        scene.close();
    }

    public void saveLayerParameterHandler(MouseEvent mouseEvent) {
        try {
            Integer oldParameterIndex = parametersList.indexOf(parameter);

            mapWindowFieldsToParameter();

            // Save parameter if new
            if(isNewParameter) {
                this.parametersList.add(parameter);
            } else {
                this.parametersList.remove(oldParameterIndex);
                this.parametersList.add(parameter);
            }

            // Close modal
            Stage scene = (Stage) this.lppName.getScene().getWindow();
            scene.close();
            LOGGER.trace("Paramater saved: " + this.lppName.getText());
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot save parameter", e);
        }
    }
}
