package helpers;

import model.Layer;
import model.Structure;
import org.apache.log4j.Logger;


public class StructureHelper {
    private static final Logger LOGGER = Logger.getLogger( StructureHelper.class.getName() );

    public static void addLayer(Structure structure, Layer layer) {
        LOGGER.trace("Layer to add: " + layer.toString());
        structure.getLayers().add(layer);
    }

    public static void deleteLayer(Structure structure, int index) {
        LOGGER.trace("Layers before Delete: " + structure.getLayers().size());
        structure.getLayers().remove(index);
        LOGGER.trace("Layers after Delete: " + structure.getLayers().size());
    }

    public static void moveDownLayer(Structure structure, Integer layerIndex) {
        LOGGER.trace("LayerIndex " + layerIndex);
        if(layerIndex < structure.getLayers().size() - 1) {
            Layer old = structure.getLayers().get(layerIndex);
            structure.getLayers().set(layerIndex, structure.getLayers().get(layerIndex + 1));
            structure.getLayers().set(layerIndex + 1, old);
        }
    }

    public static void moveUpLayer(Structure structure, Integer layerIndex) {
        LOGGER.trace("LayerIndex " + layerIndex);
        if(layerIndex > 0) {
            Layer old = structure.getLayers().get(layerIndex);
            structure.getLayers().set(layerIndex, structure.getLayers().get(layerIndex - 1));
            structure.getLayers().set(layerIndex - 1, old);
        }
    }

    public void recalculateTotalThickness(Structure structure) {
        Double thickness = 0.0;
        for(Layer layer : structure.getLayers()) {
            thickness += layer.getThickness();
        }
        structure.setTotalThickness(thickness);
        LOGGER.trace("Total Thickness: " + thickness);
    }

    public Double getTotalThickness(Structure structure) {
        recalculateTotalThickness(structure);
        return structure.getTotalThickness();
    }
}