package helpers;

import controllers.SimulationController;
import controllers.SimulationEngineListElementController;
import ebd.Calculate;
import controllers.ProgressDialogController;
import engine.DiagramEngineController;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.cell.TreeItemPropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.CalculationResult;
import model.Layer;
import model.Parameter;
import model.SimulationSettings;
import org.apache.log4j.Logger;
import utils.*;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class SimulationWindowHelper {
    private static final Logger LOGGER = Logger.getLogger(SimulationWindowHelper.class.getName());

    SimulationController controller;

    public SimulationWindowHelper(SimulationController controller) {
        this.controller = controller;
    }


    public void updateSeriesTree() {
        TreeItem rootItem = new TreeItem("Parent");
        rootItem.setExpanded(true);

        for(DiagramEngineController dec : controller.getDiagramEngineControllers()) {
            if(dec.getCalculationResults().isEmpty()) continue;

            TreeItem controllerItem = new TreeItem(dec.getEngineName());
            controllerItem.setExpanded(true);
            rootItem.getChildren().add(controllerItem);

            HashMap<String, ArrayList<CalculationResult>> calculationResultMap = new HashMap<>();
            for (CalculationResult cr : dec.getCalculationResults()) {
                UTIL.SimulationType simulationType = UTIL_SimulationWindowController.getSimulationTypeFromString((String) this.controller.simulationTypePicklist.getValue());
                if (cr.getSimulationType() != simulationType) continue;

                if (calculationResultMap.containsKey(cr.getMaterialName())) {
                    calculationResultMap.get(cr.getMaterialName()).add(cr);
                } else {
                    ArrayList<CalculationResult> crList = new ArrayList<>();
                    crList.add(cr);
                    calculationResultMap.put(cr.getMaterialName(), crList);
                }
            }

            for(String layerName : calculationResultMap.keySet()) {
                TreeItem layerItem = new TreeItem(layerName);
                layerItem.setExpanded(true);
                controllerItem.getChildren().add(layerItem);
                for(CalculationResult calcRes : calculationResultMap.get(layerName)) {
                    TreeItem seriesItem = new TreeItem(calcRes);
                    seriesItem.setExpanded(true);
                    layerItem.getChildren().add(seriesItem);
                }
            }
        }

        Platform.runLater(() -> {
            controller.visibleSeriesTree.setRoot(rootItem);
            controller.visibleSeriesTree.setCellFactory(e -> new CustomSeriesCell());
            controller.visibleSeriesTree.setShowRoot(false);
        });
    }

    class CustomSeriesCell extends TreeCell<Object> {
        @Override
        protected void updateItem(Object item, boolean empty) {
            super.updateItem(item, empty);

            // If the cell is empty we don't show anything.
            if (isEmpty()) {
                setGraphic(null);
                setText(null);
            } else {
                // We only show the custom cell if it is a leaf, meaning it has
                // no children.
                if(item instanceof String) {
                    setText((String)item);
                } else if(item instanceof CalculationResult) {
                    CalculationResult cr = (CalculationResult) item;
                    // A custom HBox that will contain your check box, label and
                    // button.
                    HBox cellBox = new HBox(5);

                    CheckBox checkBox = new CheckBox();
                    checkBox.setSelected(!cr.getHideSeries());
                    checkBox.selectedProperty().addListener(
                            new ChangeListener<Boolean>() {
                                @Override
                                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                                    cr.setHideSeries(oldValue);
                                    updateChartSeries();
                                }
                            }
                    );

                    Label label = new Label(cr.getChartSeries().getName());
                    // Here we bind the pref height of the label to the height of the checkbox. This way the label and the checkbox will have the same size.
                    label.prefHeightProperty().bind(checkBox.heightProperty());

                    cellBox.getChildren().addAll(checkBox, label);

                    setGraphic(cellBox);
                    setText(null);
                } else {
                    setText("?");
                }

            }
        }
    }

    public void prepareParametersTree() {
        TreeTableView<Parameter> layersViewTree = this.controller.layersViewTree;
        TreeTableColumn<Parameter, String> treeTableColumn1 = new TreeTableColumn<>("Name");
        TreeTableColumn<Parameter, String> treeTableColumn2 = new TreeTableColumn<>("Symbol");
        TreeTableColumn<Parameter, String> treeTableColumn3 = new TreeTableColumn<>("Value");
        TreeTableColumn<Parameter, String> treeTableColumn4 = new TreeTableColumn<>("Unit");

        treeTableColumn1.setCellValueFactory(new TreeItemPropertyValueFactory<>("Name"));
        treeTableColumn2.setCellValueFactory(new TreeItemPropertyValueFactory<>("Symbol"));
        treeTableColumn3.setCellValueFactory(new TreeItemPropertyValueFactory<>("ValueString"));
        treeTableColumn4.setCellValueFactory(new TreeItemPropertyValueFactory<>("Unit"));

        layersViewTree.getColumns().add(treeTableColumn1);
        layersViewTree.getColumns().add(treeTableColumn2);
        layersViewTree.getColumns().add(treeTableColumn3);
        layersViewTree.getColumns().add(treeTableColumn4);

        // Real preparations
        TreeItem topLevelTreeItem = new TreeItem(new Parameter("-", "-", "-", null, null, false, false));
        List<Layer> layersList = UTIL_Runtime.MODEL_STRUCTURE.getLayers();
        for (Layer layer : layersList) {
            TreeItem layerContainerLevel = new TreeItem(new Parameter("", "", layer.getName(), null, null, false, false));
            topLevelTreeItem.getChildren().add(layerContainerLevel);
            for (Parameter param : layer.getMaterial().getParameters().values()) {
                TreeItem singleParam = new TreeItem(param);
                layerContainerLevel.getChildren().add(singleParam);
            }
        }
        layersViewTree.setRoot(topLevelTreeItem);
        layersViewTree.setShowRoot(false);

        layersViewTree.getSelectionModel().selectedItemProperty().addListener( new ChangeListener() {

            @Override
            public void changed(ObservableValue observable, Object oldValue,
                                Object newValue) {

                TreeItem<Parameter> selectedItem = (TreeItem<Parameter>) newValue;



                if(selectedItem == null) return;;

                Parameter p = selectedItem.getValue();
                controller.parameterValueTF.setText(p.getValueString());

                if (p == null || !p.getEditable()) {
                    controller.parameterValueTF.setDisable(true);
                    controller.saveParameterBtn.setDisable(true);
                } else {
                    controller.parameterValueTF.setDisable(false);
                    controller.saveParameterBtn.setDisable(false);
                }
            }
        });
    }

    public void initSimulationModeChangeAction() {
        // Set action on change
        this.controller.simulationTypePicklist.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String old, String newValue) {
                UTIL.SimulationType st = UTIL_SimulationWindowController.getSimulationTypeFromString(newValue);
                // Update series labels and chart  title
                switch (st) {
                    case ENERGY:
                        controller.chart.setTitle("Energy");
                        controller.chart.getYAxis().setLabel("Energy (eV)");
                        break;
                    case POTENTIAL:
                        controller.chart.setTitle("Potential");
                        controller.chart.getYAxis().setLabel("Potential (V)");
                        break;
                    case ELECTRIC_FIELD:
                        controller.chart.setTitle("Electric field");
                        controller.chart.getYAxis().setLabel("Electric Field (MV/cm)");
                        break;
                    case CHARGE_DENSITY:
                        controller.chart.setTitle("Charge density");
                        controller.chart.getYAxis().setLabel("Charge Density (C/cm2)");
                        break;
                    default:
                        // No default value
                }

                controller.chart.getXAxis().setLabel("Distance (nm)");

                // Refresh view

                controller.chart.getData().clear();
                controller.chart.autosize();
                updateChartSeries();
                updateSeriesTree();
            }
        });
    }

    public void updateChartSeries() {
        HashMap<String, XYChart.Series> seriesToUpdate = new HashMap<>();
        for (DiagramEngineController engineController : controller.getDiagramEngineControllers()) {
            String seriesSuffix = " (" + engineController.getEngineName() + ")";

            for (CalculationResult cr : engineController.getCalculationResults()) {
                UTIL.SimulationType simulationType = UTIL_SimulationWindowController.getSimulationTypeFromString((String) this.controller.simulationTypePicklist.getValue());
                if (cr.getSimulationType() != simulationType) continue;
                if (!cr.getHideSeries()) {
                    XYChart.Series s = new XYChart.Series<>();
                    s.setName(cr.getChartSeries().getName() + seriesSuffix);

                    int counter = 0;
                    double previousValue = 0.0;
                    XYChart.Series series = cr.getChartSeries();

                    ArrayList<XYChart.Data> dataList = new ArrayList<XYChart.Data>(series.getData());
                    dataList.sort(new Comparator<XYChart.Data>() {
                        @Override
                        public int compare(XYChart.Data o1, XYChart.Data o2) {
                            return ((double) o1.getXValue() > (double) o2.getXValue()) ? 0 : 1;
                        }
                    });
                    for (XYChart.Data data : dataList) {
                        if (counter == 0 || counter == series.getData().size() - 1 || series.getData().size() == 2) {
                            s.getData().add(data);
                            previousValue = (double) data.getXValue();
                        } else {
                            if (Math.abs((double) data.getXValue() - previousValue) < Double.valueOf(this.controller.pointsSpacing.getText())) {
                                continue;
                            } else {
                                s.getData().add(data);
                                previousValue = (double) data.getXValue();
                            }
                        }
                        counter++;
                    }

                    seriesToUpdate.put(s.getName(), s);
                }
            }

            // setup Markers
            List<XYChart.Data<Number,Number>> chartVerticalMarkers = new ArrayList<>();
            double previousMarker = 0;
            for(Layer layer : UTIL_Runtime.MODEL_STRUCTURE.getLayers()) {
                if(UTIL_Runtime.MODEL_STRUCTURE.getLayers().indexOf(layer) == UTIL_Runtime.MODEL_STRUCTURE.getLayers().size() - 1) {
                    break;
                }
                previousMarker += layer.getThickness() * 1e9;
                chartVerticalMarkers.add(new XYChart.Data<>(previousMarker, 0));
            }

            Platform.runLater(() -> {
                this.controller.chart.removeAllVerticalMarkers();
                this.controller.chart.addVerticalValueMarker(chartVerticalMarkers);
                if (seriesToUpdate.isEmpty()) return;
                // Workaround! JAVAFX bug
                this.controller.chart.setAnimated(false);
                // Workaround end
                //this.controller.chart.getData().addAll(seriesToUpdate.values());
                this.controller.chart.setData(FXCollections.observableArrayList(seriesToUpdate.values()));

                for(XYChart.Series series : (List<XYChart.Series>)this.controller.chart.getData()) {
                    series.getNode().setOnMouseEntered(onMouseEnteredSeriesListener);

                    Tooltip seriesLabel = new Tooltip(series.getName());
                    Tooltip.install(series.getNode(), seriesLabel);

                    for(XYChart.Data data : (List<XYChart.Data>) series.getData()) {
                        data.getNode().setOnMouseEntered(onMouseEnteredSeriesListener);
                        Tooltip seriesDetails = new Tooltip(series.getName() + "\nX = " + data.getXValue() + "\nY = " + data.getYValue());
                        Tooltip.install(data.getNode(), seriesDetails);
                    }
                }
            });
        }
    }

    //Lambda expression
    EventHandler<MouseEvent> onMouseEnteredSeriesListener =
            (MouseEvent event) -> {
                ((Node)(event.getSource())).setCursor(Cursor.CROSSHAIR);
            };

    public void runSimulation() {
        Platform.setImplicitExit(false);

        Double temperature = Double.valueOf(this.controller.temperature.getText());
        Double voltage = Double.valueOf(this.controller.voltage.getText());

        SimulationSettings ss = new SimulationSettings(voltage, temperature,false, UTIL_SimulationWindowController.getSimulationTypeFromString((String) this.controller.simulationTypePicklist.getValue()));

        List<Calculate> calculateList = new ArrayList<>();
        for (DiagramEngineController controller : this.controller.getDiagramEngineControllers()) {
            if (controller.isEnabled()) {
                calculateList.add(new Calculate(controller, this.controller, ss));
            }
        }

        LOGGER.trace("Launching: ProgressDialogController for engines: " + calculateList.size());


        FXMLLoader loader;
        try {
            loader = new FXMLLoader(getClass().getResource("/view/progressDialog.fxml"));
            VBox pane = loader.load();
            ProgressDialogController controller = (ProgressDialogController) loader.getController();
            controller.prepareCalculation(UTIL_Runtime.MAIN_WINDOW_STAGE, calculateList);

            Scene scene = new Scene(pane);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Progress monitor");
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load ProgressDialogController Scene", e);
        }

    }

    public void loadEngineList() {
        UTIL_SimulationWindowController.loadCalculationEngines();
        this.controller.engineList.setCellFactory(property -> new SimulationEngineListElementController());
        this.controller.engineList.getItems().addAll(UTIL_Runtime.ENGINE_CONTROLLER_HANDLERS.values());
        this.controller.getDiagramEngineControllers().addAll(UTIL_Runtime.ENGINE_CONTROLLER_HANDLERS.values());
    }
}