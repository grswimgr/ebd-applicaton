package helpers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.MaterialPropertiesController;
import controllers.StructureComposerController;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.*;
import utils.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

public class StructureComposerHelper {
    private static final Logger LOGGER = Logger.getLogger( StructureComposerHelper.class.getName() );

    StructureComposerController controller;

    public StructureComposerHelper(StructureComposerController controller) {
        this.controller = controller;
    }

    public static HashMap<String, Material> loadMaterials() {
        String path = UTIL_Runtime.APPLICATION_SETTINGS.getProperty("composer.materials");
        try {
            String materialsContent = UTIL_DataTransfer.getFileContent(path);
            ObjectMapper mapper = new ObjectMapper();
            UTIL_Runtime.MATERIALS_MAP = mapper.readValue(materialsContent, new TypeReference<HashMap<String, Material>>() {});
            return UTIL_Runtime.MATERIALS_MAP;
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Materials list!", e);
        }
        return null;
    }

    public void initMaterialTables() {
        ArrayList<Material> materials;

        if(UTIL_Runtime.MATERIALS_MAP == null || UTIL_Runtime.MATERIALS_MAP.isEmpty()) {
            materials = new ArrayList<Material>(loadMaterials().values());
        } else {
            materials = new ArrayList<Material>(UTIL_Runtime.MATERIALS_MAP.values());
        }

        List<Material> metals = new ArrayList<Material>();
        List<Material> semiconductors = new ArrayList<Material>();
        List<Material> dielectrics = new ArrayList<Material>();

        for(Material m : materials) {
            switch (m.getType()) {
                case METAL:
                    metals.add(m);
                    break;
                case DIELECTRIC:
                    dielectrics.add(m);
                    break;
                case SEMICONDUCTOR:
                    semiconductors.add(m);
                    break;
                default: break;
            }
        }

        this.controller.strCmpMetalTable.getItems().clear();
        this.controller.strCmpDielectricTable.getItems().clear();
        this.controller.strCmpSemiconductorTable.getItems().clear();
        this.controller.strCmpMetalTable.setItems(FXCollections.observableArrayList(metals));
        this.controller.strCmpDielectricTable.setItems(FXCollections.observableArrayList(dielectrics));
        this.controller.strCmpSemiconductorTable.setItems(FXCollections.observableArrayList(semiconductors));
    }

    public void setStructureColumns() {
        List<TableColumn> structureColumns = (List<TableColumn>)this.controller.strCmpTable.getColumns();
        structureColumns.get(0).setCellValueFactory(new PropertyValueFactory<Layer, String>("name"));

        structureColumns.get(1).setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Layer, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Layer, String> layer) {
                if (layer.getValue().getMaterial().getName() != null) {
                    return new SimpleStringProperty(String.valueOf(layer.getValue().getMaterial().getName()));
                } else {
                    return new SimpleStringProperty("<unknown>");
                }
            }
        });

        structureColumns.get(2).setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Layer, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Layer, String> mat) {
                if (mat.getValue().getMaterial().getParameters() != null) {
                    Parameter p = mat.getValue().getMaterial().getParameters().get("{thickness}");
                    if(p == null || p.getUnit() == null) return new SimpleStringProperty("Token: {thickness} is missing!");
                    return new SimpleStringProperty(String.valueOf(p.getValue()) + " [" + p.getUnit() + "]");
                } else {
                    return new SimpleStringProperty("Token: {thickness} is missing!");
                }
            }
        });

        structureColumns.get(3).setCellValueFactory(new PropertyValueFactory<Layer, String>("description"));
    }

    public void setSemiconductorColumns() {
        List<TableColumn> semiconductorColumns = (List<TableColumn>)this.controller.strCmpSemiconductorTable.getColumns();
        semiconductorColumns.get(0).setCellValueFactory(new PropertyValueFactory<Material, String>("name"));
        semiconductorColumns.get(1).setCellValueFactory(new PropertyValueFactory<Material, String>("description"));
    }

    public void setDielectricColumns() {
        List<TableColumn> dielectricColumns = (List<TableColumn>)this.controller.strCmpDielectricTable.getColumns();
        dielectricColumns.get(0).setCellValueFactory(new PropertyValueFactory<Material, String>("name"));
        dielectricColumns.get(1).setCellValueFactory(new PropertyValueFactory<Material, String>("description"));
    }

    public void setMetalColumns() {
        List<TableColumn> metalColumns = (List<TableColumn>)this.controller.strCmpMetalTable.getColumns();
        metalColumns.get(0).setCellValueFactory(new PropertyValueFactory<Material, String>("name"));
        metalColumns.get(1).setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Material, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<Material, String> mat) {
                if (mat.getValue().getParameters() != null && mat.getValue().getParameters().containsKey("{workFunction}")) {
                    return new SimpleStringProperty(String.valueOf(mat.getValue().getParameterValue("{workFunction}")));
                } else {
                    return new SimpleStringProperty("Token: {workFunction} is missing!");
                }
            }
        });
        metalColumns.get(2).setCellValueFactory(new PropertyValueFactory<Material, String>("description"));
    }

    public void initStructureRowMenu() {
        this.controller.strCmpTable.setRowFactory( tv -> {
            TableRow<Layer> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                this.controller.handleTableRightClick(event, this.controller.strCmpTable, row.getItem(), this.controller.structureRowMenu);

                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    openLayerEditor();
                }
            });

            return row ;
        });

        this.controller.structureRowMenu = new ContextMenu();

        MenuItem moveUpLayerItem = new MenuItem("Move Up");
        moveUpLayerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LOGGER.trace("Handle MOVE UP action for: " + controller.structureRowMenu.getOwnerNode().getId());
                TableView tw = controller.getTablesByNameMap().get(controller.structureRowMenu.getOwnerNode().getId());
                Integer focusedIndex = tw.getSelectionModel().getFocusedIndex();
                LOGGER.trace("Focused Index: " + focusedIndex);
                StructureHelper.moveUpLayer(UTIL_Runtime.MODEL_STRUCTURE, focusedIndex);
                refreshCmpTable();
                refreshStructureChart();
            }
        });

        MenuItem moveDownLayerItem = new MenuItem("Move Down");

        moveDownLayerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LOGGER.trace("Handle MOVE DOWN action for: " + controller.structureRowMenu.getOwnerNode().getId());
                TableView tw = controller.getTablesByNameMap().get(controller.structureRowMenu.getOwnerNode().getId());
                Integer focusedIndex = tw.getSelectionModel().getFocusedIndex();
                LOGGER.trace("Focused Index: " + focusedIndex);
                StructureHelper.moveDownLayer(UTIL_Runtime.MODEL_STRUCTURE, focusedIndex);
                refreshCmpTable();
                refreshStructureChart();
            }
        });

        MenuItem editItem = new MenuItem("Edit layer");
        editItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                openLayerEditor();
            }
        });

        MenuItem deleteLayerItem = new MenuItem("Delete Layer");
        deleteLayerItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LOGGER.trace("Handle DELETE action for: " + controller.structureRowMenu.getOwnerNode().getId());
                TableView tw = controller.getTablesByNameMap().get(controller.structureRowMenu.getOwnerNode().getId());
                Integer focusedIndex = tw.getSelectionModel().getFocusedIndex();
                LOGGER.trace("Focused Index: " + focusedIndex);

                StructureHelper.deleteLayer(UTIL_Runtime.MODEL_STRUCTURE, focusedIndex);

                refreshCmpTable();
                refreshStructureChart(); 
            }
        });
        this.controller.structureRowMenu.getItems().addAll(moveUpLayerItem, moveDownLayerItem, editItem, deleteLayerItem);
    }

    public void refreshCmpTable() {
        LOGGER.trace("Handle: refreshCmpTable");

        this.controller.strCmpTable.getItems().clear();
        this.controller.strCmpTable.setItems(FXCollections.observableArrayList(UTIL_Runtime.MODEL_STRUCTURE.getLayers()));
    }

    public void refreshStructureChart() {
        try {
            LOGGER.trace("Handle: refreshCmpTable");

            List<XYChart.Series<String, Number>> chartLayers = new ArrayList<XYChart.Series<String, Number>>();

            for (Layer layer : this.controller.getStructure().getLayers()) {
                XYChart.Series<String, Number> series = new XYChart.Series<>();
                series.setName(layer.getName());

                // TODO
                Parameter p = layer.getMaterial().getParameters().get("{thickness}");
                if(p == null) {
                    LOGGER.error("CANNOT REFRESH CHART FOR: " + layer.getName() + ". Parameter token: {thickness} not found!");
                    continue;
                }

                series.getData().add(new XYChart.Data<>(this.controller.getStructure().getName(), p.getValue()));
                System.out.println("Name: " + layer.getName() + " thickness: " + layer.getThickness());
                chartLayers.add(series);
            }

            this.controller.structureChart.setData(FXCollections.observableArrayList(chartLayers));
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to refresh Structure Preview",  e);
        }
    }

    public void openMaterialEditor(Material material) {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/materialProperties.fxml"));
            VBox pane = root.load();
            MaterialPropertiesController mpc = root.getController();
            mpc.initData(material, this.controller);
            Stage secondStage  = new Stage();
            secondStage.initModality(Modality.APPLICATION_MODAL);
            secondStage.setScene(new Scene(pane));
            secondStage.show();
        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Unable to load Material Properties Scene", e);
        }
    }

    public void openLayerEditor() {
        Integer focusedIndex = this.controller.strCmpTable.getSelectionModel().getFocusedIndex();
        LOGGER.trace("Focused Index: " + focusedIndex);
        controller.openLayerPropertiesWindow(UTIL_Runtime.MODEL_STRUCTURE.getLayers().get(focusedIndex));
        refreshCmpTable();
        refreshStructureChart();
    }
}