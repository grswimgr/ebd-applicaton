package helpers;

import controllers.StructureSelectorController;
import controllers.StructureComposerController;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import model.Structure;
import utils.UTIL_Methods;
import utils.UTIL_Runtime;
import utils.UTIL_StructureComposer;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class SelectStructureWindowHelper {
    StructureSelectorController ssc;

    public SelectStructureWindowHelper(StructureSelectorController ssc) {
        this.ssc = ssc;
    }

    public static File[] prepareRecentlyUsedFiles() {
        List<File> recentlyUsedFiles = new ArrayList<File>();
        File[] recentlyUsedFilesArray = {};

        String recentFilesPath = "recentlyUsed.txt";

        Path path = Paths.get(recentFilesPath);

        try (Stream<String> lines = Files.lines(path)) {
            lines.forEachOrdered(line-> {
                File f = new File(line);
                if(f.lastModified() != 0) recentlyUsedFiles.add(f);
            });

            recentlyUsedFilesArray = new File[recentlyUsedFiles.size()];
            recentlyUsedFilesArray = recentlyUsedFiles.toArray(recentlyUsedFilesArray);
            Arrays.sort(recentlyUsedFilesArray, new Comparator<File>() {
                @Override
                public int compare(File f1, File f2) {
                    return Long.compare(f2.lastModified(), f1.lastModified());
                }
            });

        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot open recently used file", e);
        }

        return recentlyUsedFilesArray;
    }

    public void fillRecentlyUsedStructuresTable() {
        File[] files = prepareRecentlyUsedFiles();

        TableView tab = this.ssc.recentlyUsedTable;
        List<TableColumn> columns = tab.getColumns();

        columns.get(0).setCellValueFactory(new Callback<TableColumn.CellDataFeatures<File, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<File, String> f) {
                return new ReadOnlyObjectWrapper(f.getValue().getName());
            }
        });

        columns.get(1).setCellValueFactory(new Callback<TableColumn.CellDataFeatures<File, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(TableColumn.CellDataFeatures<File, String> f) {
                DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(f.getValue().lastModified());
                return new ReadOnlyObjectWrapper(df.format(cal.getTime()));
            }
        });

        tab.getItems().addAll(files);
    }

    public void initDoubleClickOnRecentluUsedTable() {
        TableView table = this.ssc.recentlyUsedTable;
        table.setRowFactory( tv -> {
            TableRow<File> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Structure newStructure = UTIL_StructureComposer.loadStructureFromFile(row.getItem());
                    if(newStructure == null) return;
                    UTIL_Runtime.MODEL_STRUCTURE = newStructure;

                    openStructureEditor();
                }
            });
            return row ;
        });
    }

    public void openStructureEditor() {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/structureComposer.fxml"));
            VBox pane = root.load();
            StructureComposerController structureComposeController = root.getController();
            structureComposeController.initData();

            UTIL_Runtime.STRUCTURE_COMPOSER_SCENE = new Scene(pane);
            UTIL_Runtime.MAIN_WINDOW_STAGE.setScene(UTIL_Runtime.STRUCTURE_COMPOSER_SCENE);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
