package helpers;

import controllers.EngineSelectorController;
import controllers.EngineSelectorListElementController;
import controllers.StructureSelectorController;
import engine.DiagramEngineController;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import org.apache.log4j.Logger;
import utils.UTIL_Methods;
import utils.UTIL_Runtime;

import java.io.*;
import java.nio.file.Files;
import java.util.*;


public class SelectEngineWindowHelper {
    private static final Logger LOGGER = Logger.getLogger( SelectEngineWindowHelper.class.getName() );

    public static class SelectEngineException extends Exception {
        public SelectEngineException(String message) {
            super(message);
        }
    }

    EngineSelectorController esc;
    List<Properties> enginesProperties = new ArrayList<>();

    public SelectEngineWindowHelper(EngineSelectorController esc) {
        LOGGER.info("Initializing SelectEngineWindowHelper...");
        this.esc = esc;
    }

    public List<Properties> loadEngines(List<File> filesList) {
        List<Properties> enginesProperties = new ArrayList<>();
        for(File file : filesList) {
            enginesProperties.add(UTIL_Methods.loadProperties(file.getPath()));
        }
        return enginesProperties;
    }

    public void prepareEnginesList() {
        this.enginesProperties = loadEngines(prepareEnginesPropertiesFilesList());

        this.esc.engineList.setCellFactory(property -> new EngineSelectorListElementController());

        this.esc.engineList.getItems().addAll(this.enginesProperties);


        this.esc.engineList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                esc.launchSimulator.setDisable(false);
                loadModelDetails((Properties) esc.engineList.getSelectionModel().getSelectedItem());
            }
        });
    }

    public void loadModelDetails(Properties properties) {
        this.esc.engineNameLabel.setText(
            (properties.containsKey("engine.name")) ? properties.getProperty("engine.name") : "-"
        );
        this.esc.engineAuthorLabel.setText(
                (properties.containsKey("engine.author")) ? properties.getProperty("engine.author") : "-"
        );
        this.esc.engineDescriptionLabel.setText(
                (properties.containsKey("engine.description")) ? properties.getProperty("engine.description") : "-"
        );
    }

    public List<File> prepareEnginesPropertiesFilesList() {
        List<File> propertiesFilesToProcess = new ArrayList<>();
        try {
            LOGGER.info("Searching available Engines...");
            Files.find(new File("engines").toPath(),Integer.MAX_VALUE,(filePath, fileAttr) -> fileAttr.isDirectory()).forEach(
                path -> {
                    File[] propertiesFiles = path.toFile().listFiles(new FilenameFilter() {
                        public boolean accept(File dir, String name) {
                            return name.endsWith("properties");
                        }
                    });
                    propertiesFilesToProcess.addAll(Arrays.asList(propertiesFiles));
                }
            );

            LOGGER.debug("Found engines properties:");
            for(File file : propertiesFilesToProcess) {
                LOGGER.debug(file.getPath());
            }

        } catch (IOException e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot prepare Engine properties File!", e);
        }
        return propertiesFilesToProcess;
    }

    public void loadAllEngines() {
        loadAllEngines(this.esc.engineList.getItems());
        openStructureSelector();
    }

    public void loadAllEngines(List<Properties> propertiesList) {
        try {
            for(Object props : propertiesList) {
                Properties properties = (Properties) props;
                DiagramEngineController loadedController = null;

                if(!properties.containsKey("engine.jar")) throw new SelectEngineException("Invalid engine.properties file! Engine JAR path (engine.jar) is not available!");
                if(!properties.containsKey("engine.name")) throw new SelectEngineException("Invalid engine.properties file! Engine name (engine.name) is not available!");

                if(properties.getProperty("engine.jar").startsWith("/")){
                    loadedController = UTIL_Methods.generateDiagramEngineController(properties.getProperty("engine.jar"), properties.getProperty("engine.name"));
                } else {
                    loadedController = UTIL_Methods.generateDiagramEngineController("engines/" + properties.getProperty("engine.jar"), properties.getProperty("engine.name"));
                }
                loadedController.setProperties((Properties) props);
            }
        } catch (Exception e) {
            UTIL_Methods.handleExceptionShowAlert(Alert.AlertType.ERROR, "Cannot load engine!", e);
        }
    }

    public void openStructureSelector() {
        FXMLLoader root;
        try {
            root = new FXMLLoader(getClass().getResource("/view/selectStructureWindow.fxml"));
            VBox pane = root.load();
            StructureSelectorController ssc = root.getController();

            UTIL_Runtime.SELECT_STRUCTURE_SECENE = new Scene(pane);
            UTIL_Runtime.MAIN_WINDOW_STAGE.setScene(UTIL_Runtime.SELECT_STRUCTURE_SECENE);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
