package model;

public class Parameter {
    private String token;
    private String symbol;
    private String name;
    private String unit;
    private String valueString;
    private double value;
    private Boolean editable;
    private Boolean isExpression;

    public Parameter(){
        this.editable = false;
        this.isExpression = false;
    }

    public Parameter(String token, String symbol, String name, String unit) {
        this.token = token;
        this.symbol = symbol;
        this.name = name;
        this.unit = unit;
        this.editable = false;
        this.isExpression = false;
    }

    public Parameter(String token, String symbol, String name, String unit, String valueString, Boolean editable, Boolean isExpression) {
        this.token = token;
        this.symbol = symbol;
        this.name = name;
        this.unit = unit;
        this.valueString = valueString;
        this.editable = editable;
        this.isExpression = isExpression;

        if(!isExpression && valueString != null && !valueString.equals("")) {
            this.value = Double.valueOf(valueString);
        } else {
            this.value = 0.0;
        }
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.isExpression = false;
        this.value = value;
    }

    public void setValue(String value) {
        this.valueString = value;

        try {
            this.value = Double.valueOf(value);
        } catch (Exception e) {
            System.out.println("Parameter value is not double. Leaving String...");
        }

    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Boolean getExpression() {
        return isExpression;
    }

    public void setExpression(Boolean expression) {
        isExpression = expression;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "token='" + token + '\'' +
                ", symbol='" + symbol + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", valueString='" + valueString + '\'' +
                ", value=" + value +
                ", editable=" + editable +
                ", isExpression=" + isExpression +
                '}';
    }
}

