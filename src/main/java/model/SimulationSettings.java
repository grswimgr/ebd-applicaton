package model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import utils.UTIL;

public class SimulationSettings {
    Double dataFrom;
    Double dataTo;
    Integer stepsNumber;
    Boolean enforceCacheClear;
    UTIL.SimulationType simulationType;
    Double voltage;
    Double temperature;

    public SimulationSettings(Double voltage, Double temperature, Boolean enforceCacheClear, UTIL.SimulationType simulationType) {
        this.voltage = voltage;
        this.temperature = temperature;
        this.enforceCacheClear = enforceCacheClear;
        this.simulationType = simulationType;
    }

    public Double getDataFrom() {
        return dataFrom;
    }

    public void setDataFrom(Double dataFrom) {
        this.dataFrom = dataFrom;
    }

    public Double getDataTo() {
        return dataTo;
    }

    public void setDataTo(Double dataTo) {
        this.dataTo = dataTo;
    }

    public Integer getStepsNumber() {
        return stepsNumber;
    }

    public void setStepsNumber(Integer stepsNumber) {
        this.stepsNumber = stepsNumber;
    }

    public Boolean getEnforceCacheClear() {
        return enforceCacheClear;
    }

    public void setEnforceCacheClear(Boolean enforceCacheClear) {
        this.enforceCacheClear = enforceCacheClear;
    }

    public UTIL.SimulationType getSimulationType() {
        return simulationType;
    }

    public void setSimulationType(UTIL.SimulationType simulationType) {
        this.simulationType = simulationType;
    }

    public Double getVoltage() {
        return voltage;
    }

    public void setVoltage(Double voltage) {
        this.voltage = voltage;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }
}
