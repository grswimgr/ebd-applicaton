package model;

import javafx.scene.chart.XYChart;
import utils.UTIL;

public class CalculationResult {
    private XYChart.Series chartSeries;
    private Exception exception;
    private String errorMessage;
    private UTIL.MaterialType type;
    private UTIL.SimulationType simulationType;
    private String materialName;
    private Boolean hideSeries;

    public CalculationResult(XYChart.Series chartSeries, Exception exception, String errorMessage, UTIL.MaterialType type, UTIL.SimulationType simulationType, String materialName, Boolean hideSeries) {
        this.chartSeries = chartSeries;
        this.exception = exception;
        this.errorMessage = errorMessage;
        this.type = type;
        this.simulationType = simulationType;
        this.materialName = materialName;
        this.hideSeries = hideSeries;
    }

    public XYChart.Series getChartSeries() {
        return chartSeries;
    }

    public void setChartSeries(XYChart.Series chartSeries) {
        this.chartSeries = chartSeries;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public UTIL.MaterialType getType() {
        return type;
    }

    public void setType(UTIL.MaterialType type) {
        this.type = type;
    }

    public UTIL.SimulationType getSimulationType() {
        return simulationType;
    }

    public void setSimulationType(UTIL.SimulationType simulationType) {
        this.simulationType = simulationType;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public Boolean getHideSeries() {
        return hideSeries;
    }

    public void setHideSeries(Boolean hideSeries) {
        this.hideSeries = hideSeries;
    }

    @Override
    public String toString() {
        return "CalculationResult{" +
                "chartSeries=" + chartSeries +
                ", exception=" + exception +
                ", errorMessage='" + errorMessage + '\'' +
                ", type=" + type +
                ", simulationType=" + simulationType +
                ", materialName='" + materialName + '\'' +
                ", hideSeries=" + hideSeries +
                '}';
    }
}
