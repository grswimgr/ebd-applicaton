package model;


public class ExtraCharge {
    private double position;
    private double chargeValue;

    public ExtraCharge(double position, double chargeValue) {
        this.position = position;
        this.chargeValue = chargeValue;
    }

    public double getPosition() {
        return position;
    }

    public void setPosition(double position) {
        this.position = position;
    }

    public double getChargeValue() {
        return chargeValue;
    }

    public void setChargeValue(double chargeValue) {
        this.chargeValue = chargeValue;
    }

    @Override
    public String toString() {
        return "ExtraCharge{" +
                "position=" + position +
                ", chargeValue=" + chargeValue +
                '}';
    }
}
