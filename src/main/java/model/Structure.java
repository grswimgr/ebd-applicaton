package model;

import java.util.ArrayList;
import java.util.List;

public class Structure {
    private String name;
    private String description;
    private ArrayList<Layer> layers;
    private Double totalThickness;

    public Structure() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Layer> getLayers() {
        return layers;
    }

    public void setLayers(ArrayList<Layer> layers) {
        this.layers = layers;
    }

    public void setTotalThickness(Double totalThickness) {
        this.totalThickness = totalThickness;
    }

    public Double getTotalThickness() {
        return totalThickness;
    }

    @Override
    public String toString() {
        return "Structure{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", layers=" + layers +
                ", totalThickness=" + totalThickness +
                '}';
    }
}
