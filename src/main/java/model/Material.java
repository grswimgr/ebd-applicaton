package model;

import utils.UTIL;
import utils.UTIL_Methods;

import java.util.HashMap;

public class Material {
    public static class MaterialException extends Exception {
        public MaterialException(String message) {
            super(message);
        }
    }

    private String name;
    private String description;
    private UTIL.MaterialType type;
    private HashMap<String, Parameter> parameters; // Key is token value for {XX} key is XX

    public Material() {
        this.parameters = new HashMap<>();
    }

    public Double getParameterValue(String name) {
        System.out.println(parameters);
        if(parameters.containsKey(name)) {
            return parameters.get(name).getValue();
        } else {
            return null;
        }
    }

    public void putParameter(String token, Parameter parameter) {
        parameters.put(token, parameter);
    }

    public Parameter getParameterByToken(String token) throws MaterialException{
        if(parameters.containsKey(token)) {
            return parameters.get(token);
        } else {
            throw new MaterialException("Parameter: " + token +  " not found!");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UTIL.MaterialType getType() {
        return type;
    }

    public void setType(UTIL.MaterialType type) {
        this.type = type;
    }

    public HashMap<String, Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, Parameter> parameters) {
        this.parameters = parameters;
    }

    @Override
    public String toString() {
        return "Material{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type=" + type +
                ", parameters=" + parameters +
                '}';
    }
}