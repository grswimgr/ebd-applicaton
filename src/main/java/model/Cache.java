package model;

import utils.UTIL;
import java.util.HashMap;
import java.util.Map;

public class Cache {
    // Cache map contains as a key Simulation Type and as a value another map with key as series name
    Map<UTIL.SimulationType, Map<String, Map<Double, Double>>> standardCacheMap;
    Map<Object, Object> customCacheMap;

    public Cache() {
        standardCacheMap = new HashMap<UTIL.SimulationType, Map<String, Map<Double, Double>>>();
        standardCacheMap.put(UTIL.SimulationType.ENERGY, new HashMap<String, Map<Double, Double>>());
        standardCacheMap.put(UTIL.SimulationType.POTENTIAL, new HashMap<String, Map<Double, Double>>());
        standardCacheMap.put(UTIL.SimulationType.CHARGE_DENSITY, new HashMap<String, Map<Double, Double>>());
        standardCacheMap.put(UTIL.SimulationType.ELECTRIC_FIELD, new HashMap<String, Map<Double, Double>>());
        customCacheMap = new HashMap<Object, Object>();
    }

    public Map<UTIL.SimulationType, Map<String, Map<Double, Double>>> getStandardCacheMap() {
        return standardCacheMap;
    }

    public void setStandardCacheMap(Map<UTIL.SimulationType, Map<String, Map<Double, Double>>> standardCacheMap) {
        this.standardCacheMap = standardCacheMap;
    }

    public Map<Object, Object> getCustomCacheMap() {
        return customCacheMap;
    }

    public void setCustomCacheMap(Map<Object, Object> customCacheMap) {
        this.customCacheMap = customCacheMap;
    }
}
